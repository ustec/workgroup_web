from django.db import models

class Config(models.Model):
    key = models.CharField(max_length=100, verbose_name='Clau', unique=True)
    value = models.CharField(max_length=100, verbose_name='Valor', blank=True)
    class Meta:
        verbose_name = 'Configuració'
        verbose_name_plural = 'Configuracions'
    def __str__(self):
        return self.key