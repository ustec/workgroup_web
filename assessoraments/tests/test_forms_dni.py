from django.test import TestCase
from assessoraments.forms import DNIForm
from assessoraments.models import CandidateDocents, CourseDocents, InstructionsDocents, TerritoryContactDocents, Territory

# Create your tests here.
class DNIFormTestCase(TestCase):
    databases = '__all__'
    def setUp(self):
        self.form = DNIForm(course_type="docents")
        InstructionsDocents.objects.create(code="ASSIGNATAFILIAT",text_email="Estàs a la cua d'espera")
        tu = Territory.objects.create(name="Barcelona")
        t = TerritoryContactDocents.objects.create(territory=tu, email="monica@sindicat.net")
        c = CourseDocents.objects.create(code="B", speciality="", territory_contact = t, city = "", place = "",
                              address = "", dates = "", vaccancies = 3, closed = False)        
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                email = "a@a.org", is_real_member = True, selected_course = c)
         
    def tearDown(self):
        Territory.objects.all().delete()
        TerritoryContactDocents.objects.all().delete()
        InstructionsDocents.objects.all().delete()
        CourseDocents.objects.all().delete()
        CandidateDocents.objects.all().delete()        
         
    def test_valid_dni(self):
        valid_dnis = ['37133774K', '96409464L', '68799819B', '26973064K', '47561342X', 
                      '86160994G', '76790659F', '99952071M', '46713742Y', '00865733J']
        invalid_dnis = ['37133774J', '96409464M', '68799819A', '26973064R', '47561342T', 
                        '86160994', 'G', '099952071M', 'ABC', '']
        valid_nies = ['Z2351287W', 'Z5293809L', 'X0259277K', 'Y4596700A', 'Y8097596T', 
                      'Y9194552V', 'Y8488255G']
        for d in valid_dnis:
            self.assertEqual(self.form.valid_dni(d), True)
        for d in invalid_dnis:
            self.assertEqual(self.form.valid_dni(d), False)
        for d in valid_nies:
            self.assertEqual(self.form.valid_dni(d), True)
              
    def test_duplicated_dni(self):
        self.assertEqual(self.form.duplicated_dni("37133774K"), True)
        self.assertEqual(self.form.duplicated_dni("96409464L"), False)
          
