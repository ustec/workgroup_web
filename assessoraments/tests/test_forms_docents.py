from django.test import TestCase
from assessoraments.forms import AssessoramentDocentsForm
from assessoraments.models import CandidateDocents, CourseDocents, InstructionsDocents, TerritoryContactDocents, Territory

# Create your tests here.
class AssessoramentFormTestCase(TestCase):
    databases = '__all__'
    def setUp(self):
        self.form = AssessoramentDocentsForm()
        InstructionsDocents.objects.create(code="ASSIGNATAFILIAT",text_email="Estàs a la cua d'espera")
        tu = Territory.objects.create(name="Barcelona")
        t = TerritoryContactDocents.objects.create(territory=tu, email="monica@sindicat.net")
        c = CourseDocents.objects.create(code="B", speciality="", territory_contact = t, city = "", place = "",
                              address = "", dates = "", vaccancies = 3, closed = False)        
        self.p1 = CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        
        
    def tearDown(self):
        Territory.objects.all().delete()
        TerritoryContactDocents.objects.all().delete()
        InstructionsDocents.objects.all().delete()
        CourseDocents.objects.all().delete()
        CandidateDocents.objects.all().delete()
    
    def test_valid_phone(self):        
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "123"
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "123a"
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "abc"
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "123 456 789"
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "1234567890"
        self.assertEqual(self.form.valid_phone(self.p1.phone), False)
        self.p1.phone = "123456789"
        self.assertEqual(self.form.valid_phone(self.p1.phone), True)
        