from django.test import TestCase

from assessoraments.models import CandidateDocents, CourseDocents, InstructionsDocents, Member, TerritoryContactDocents, Territory

class SetUp(TestCase):
    databases = '__all__'
    def setUp(self):
        tu = Territory.objects.create(name="Barcelona")
        tb = TerritoryContactDocents.objects.create(territory=tu, 
                                      email="monica@sindicat.net")
        InstructionsDocents.objects.create(code="ASSIGNATAFILIAT", 
                                    text_email="Tens plaça afiliat")
        InstructionsDocents.objects.create(code="ASSIGNATNOAFILIAT",
                                    text_email="Tens plaça NO afiliat", 
                                    text_web="Tens plaça")
        InstructionsDocents.objects.create(code="LLISTAESPERA",
                                    text_email="Estàs a la llista d'espera",
                                    text_web="Llista d'espera")
        InstructionsDocents.objects.create(code="SENSEPLACES",
                                    text_email="No tens plaça",
                                    text_web="Sense plaça")
        CourseDocents.objects.create(code="B", speciality="", territory_contact = tb, city = "", 
                              place = "", address = "", dates = "", 
                              vaccancies = 2, closed = False)
        Member.objects.create(dni="37133774K", territory=tu)
        Member.objects.create(dni="96409464L", territory=tu)
        Member.objects.create(dni="00865733J", territory=tu)
        Member.objects.create(dni="76790659F", territory=tu)
        
    def tearDown(self):
        Territory.objects.all().delete()
        TerritoryContactDocents.objects.all().delete()
        InstructionsDocents.objects.all().delete()
        CourseDocents.objects.all().delete()
        Member.objects.all().delete()        

class CandidateDocentsTestCase(SetUp):
    databases = '__all__'
    def test_get_instructions_assignat(self):
        # A member get vaccancy in a non full course
        c = CandidateDocents.objects.create(dni="37133774K", name = "Mònica", 
                                     surname1 = "Ramírez", phone = "",
                                     email = "a@a.org", is_real_member = True, 
                                     selected_course = CourseDocents.objects.get(code="B"))
        self.assertEqual(c.get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_email)
        self.assertEqual(c.get_instructions("text_web"), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_web)
        
    def test_get_instructions_last_vaccancy_assignat(self):
        # CourseDocents 'B' has two vaccancies
        # A member get vaccancy in a non full course
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", 
                                 surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, 
                                 selected_course = CourseDocents.objects.get(code="B"))
        # Another member gets the last vaccancy
        c = CandidateDocents.objects.create(dni="96409464L", name = "Mònica", 
                                      surname1 = "Ramírez", phone = "",
                                      email = "a@a.org", is_real_member = True, 
                                      selected_course = CourseDocents.objects.get(code="B"))
        self.assertEqual(c.get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_email)
        self.assertEqual(c.get_instructions("text_web"), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_web)        
        
    def test_get_instructions_notmember_llistaespera(self):
        # A non-member goes to waiting list
        c = CandidateDocents.objects.create(dni="68799819B", name = "Mònica", 
                                     surname1 = "Ramírez", phone = "",
                                     email = "a@a.org", is_real_member = False, 
                                     selected_course = CourseDocents.objects.get(code="B"))
        self.assertEqual(c.get_instructions(), 
                         InstructionsDocents.objects.get(code="LLISTAESPERA").text_email)
        self.assertEqual(c.get_instructions("text_web"), 
                         InstructionsDocents.objects.get(code="LLISTAESPERA").text_web)  
        
    def test_get_instructions_member_llistaespera(self):      
        # A member get vaccancy in a non full course
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", 
                                 surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, 
                                 selected_course = CourseDocents.objects.get(code="B"))
        # Another member gets the last vaccancy
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", 
                                 surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, 
                                 selected_course = CourseDocents.objects.get(code="B"))
        # Another member goes to waiting list because there are no vaccancies
        c = CandidateDocents.objects.create(dni="00865733J", name = "Mònica", 
                                     surname1 = "Ramírez", phone = "",
                                     email = "a@a.org", is_real_member = True, 
                                     selected_course = CourseDocents.objects.get(code="B"))
        self.assertEqual(c.get_instructions(), 
                         InstructionsDocents.objects.get(code="LLISTAESPERA").text_email)
        self.assertEqual(c.get_instructions("text_web"), 
                         InstructionsDocents.objects.get(code="LLISTAESPERA").text_web)

    def test_get_instructions_assignatllista(self):
        # CourseDocents 'B' has two vaccancies
        b = CourseDocents.objects.get(code="B")
        # A member get vaccancy in a non full course
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", 
                                      surname1 = "Ramírez", phone = "",
                                      email = "a@a.org", is_real_member = True, 
                                      selected_course = b)
        # A non-member goes to waiting list
        c2 = CandidateDocents.objects.create(dni="68799819B", name = "Mònica", 
                                      surname1 = "Ramírez", phone = "",
                                      email = "a@a.org", is_real_member = False, 
                                      selected_course = b)
        # course is closed
        b.close_vaccancies()
        c2.refresh_from_db()
        self.assertEqual(c2.get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATNOAFILIAT").text_email)
          
class CourseDocentsTestCase(SetUp):
    databases = '__all__'    
    def test_free_vaccancies(self):
        c = CourseDocents.objects.get(code="B")
        # 2 vaccancies  
        self.assertTrue(c.free_vaccancies())   
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # 1 vaccancy
        self.assertTrue(c.free_vaccancies())
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                     email = "a@a.org", is_real_member = True, selected_course = c)
        # 0 vaccancies
        self.assertFalse(c.free_vaccancies())
         
    def test_reorder_waitinglist_full_no_members(self):
        c = CourseDocents.objects.get(code="B")
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member, goes to waiting list
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member, goes to waiting list
        CandidateDocents.objects.create(dni="46713742Y", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member, goes to waiting list
        CandidateDocents.objects.create(dni="99952071M", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        c.reorder_waitinglist()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").wait_list, 1)
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").wait_list, 2)
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").wait_list, 3)
         
    def test_reorder_waitinglist_full_members(self):
        c = CourseDocents.objects.get(code="B")
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="76790659F", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        c.reorder_waitinglist()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 1)
        self.assertEqual(CandidateDocents.objects.get(dni="76790659F").wait_list, 2)   
         
    def test_reorder_waitinglist_full_members_no_members(self):
        c = CourseDocents.objects.get(code="B")
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)        
        # No member
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="46713742Y", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="99952071M", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="76790659F", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
         
        c.reorder_waitinglist()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").wait_list, 1)
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").wait_list, 2)
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").wait_list, 3)        
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 4)
        self.assertEqual(CandidateDocents.objects.get(dni="76790659F").wait_list, 5)
        
    def test_reorder_waitinglist_with_refuse(self):
        b = CourseDocents.objects.get(code="B")
        # A member get vaccancy in a non full course
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", 
                                 surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, 
                                 selected_course = b)
        # Another member gets the last vaccancy
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", 
                                 surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, 
                                 selected_course = b)
        # Another member goes to waiting list because there are no vaccancies
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", 
                                     surname1 = "Ramírez", phone = "",
                                     email = "a@a.org", is_real_member = True, 
                                     selected_course = b)
        b.reorder_waitinglist()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_email)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_email)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").get_instructions(), 
                         InstructionsDocents.objects.get(code="LLISTAESPERA").text_email)
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 1)
        c1 = CandidateDocents.objects.get(dni="37133774K")
        c1.assigned_status = "REFUSED"
        c1.save()
        b.give_vaccancy_ustec_member()
        b.reorder_waitinglist()
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").get_instructions(), 
                         InstructionsDocents.objects.get(code="ASSIGNATAFILIAT").text_email)
        self.assertEqual(c1.assigned_status, "REFUSED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 0)             
             
    def test_give_vaccancy_ustec_member(self):
        c = CourseDocents.objects.get(code="B")
        # Get vaccancy
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member, goes to waiting list
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = CourseDocents.objects.get(code="B"))
        # Get vaccancy    
        cd = CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No more vaccancies: is member but goes to waiting list
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # Assign the course to the first member of waiting list
        cd.assigned_status = 'REFUSED'
        cd.save()
        c.give_vaccancy_ustec_member()
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 0)
     
    def test_close_vaccancies_with_vaccancies(self):
        c = CourseDocents.objects.get(code="B")
        c.vaccancies = 3
        c.save()
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)      
        # No member
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="46713742Y", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="99952071M", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
         
        c.close_vaccancies()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").wait_list, 1)
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").wait_list, 2)
 
    def test_close_vaccancies_no_vaccancies(self):
        c = CourseDocents.objects.get(code="B")
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)      
        # No member
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="46713742Y", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="99952071M", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
                 
        c.close_vaccancies()
         
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").wait_list, 1)        
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").wait_list, 2)
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").wait_list, 3)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 4)
             
    def test_close_vaccancies(self):
        c = CourseDocents.objects.get(code="B")
        c.vaccancies = 3
        c.save()
        # Member
        CandidateDocents.objects.create(dni="37133774K", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)      
        # No member
        CandidateDocents.objects.create(dni="68799819B", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member    
        CandidateDocents.objects.create(dni="96409464L", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = True, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="46713742Y", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # No member
        CandidateDocents.objects.create(dni="99952071M", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="00865733J", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
        # Member
        CandidateDocents.objects.create(dni="76790659F", name = "Mònica", surname1 = "Ramírez", phone = "",
                                 email = "a@a.org", is_real_member = False, selected_course = c)
         
        c.close_vaccancies()
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="37133774K").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="96409464L").wait_list, 0)
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").assigned_status, "ASSIGNED")
        self.assertEqual(CandidateDocents.objects.get(dni="68799819B").wait_list, 0)       
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="46713742Y").wait_list, 1)
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="99952071M").wait_list, 2)
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="00865733J").wait_list, 3)
        self.assertEqual(CandidateDocents.objects.get(dni="76790659F").assigned_status, "WAITLIST")
        self.assertEqual(CandidateDocents.objects.get(dni="76790659F").wait_list, 4)
