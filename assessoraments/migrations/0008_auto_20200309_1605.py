# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-03-09 15:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessoraments', '0007_auto_20200115_1317'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='candidatedocents',
            name='surnames',
        ),
        migrations.RemoveField(
            model_name='candidatelaborals',
            name='surnames',
        ),
        migrations.AddField(
            model_name='candidatedocents',
            name='surname1',
            field=models.CharField(default='', max_length=50, verbose_name='Primer Cognom'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='candidatedocents',
            name='surname2',
            field=models.CharField(blank=True, max_length=50, verbose_name='Segon Cognom'),
        ),
        migrations.AddField(
            model_name='candidatelaborals',
            name='surname1',
            field=models.CharField(default='', max_length=50, verbose_name='Primer Cognom'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='candidatelaborals',
            name='surname2',
            field=models.CharField(blank=True, max_length=50, verbose_name='Segon Cognom'),
        ),
    ]
