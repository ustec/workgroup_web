# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-03-30 16:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assessoraments', '0011_auto_20200317_2301'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='candidatedocents',
            name='is_member',
        ),
        migrations.RemoveField(
            model_name='candidatelaborals',
            name='is_member',
        ),
    ]
