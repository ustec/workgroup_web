# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-01-13 09:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessoraments', '0004_auto_20200113_1053'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidatedocents',
            name='city',
            field=models.CharField(blank=True, max_length=100, verbose_name='Població'),
        ),
        migrations.AddField(
            model_name='candidatelaborals',
            name='city',
            field=models.CharField(blank=True, max_length=100, verbose_name='Població'),
        ),
    ]
