# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-04-12 10:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessoraments', '0019_candidatedocents_territory'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidatelaborals',
            name='territory',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='assessoraments.Territory', verbose_name='Territori'),
        ),
    ]
