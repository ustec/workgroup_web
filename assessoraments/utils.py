from django.core.mail import EmailMessage
from django.utils.html import strip_tags

def send_instructions_mail(candidate, instructions):
    territory_email = candidate.selected_course.territory_contact.email
    body = "DNI: " + candidate.dni + "\n" + \
           "Nom: " + candidate.name + " " + candidate.surname1 + " " + candidate.surname2 + "\n\n" + \
           instructions
    body = strip_tags(body)
    email = EmailMessage(
        'Assessorament Oposicions USTEC',
        body,
        territory_email,
        [candidate.email],
        [territory_email]
    )
    error = email.send()    
    return error