from django import template
from django.forms import RadioSelect

register = template.Library()

@register.filter(name='is_radio')
def is_checkbox(field):
    return field.field.widget.__class__.__name__ == RadioSelect().__class__.__name__