from django.contrib import admin
from .models import Member, CandidateDocents, CourseDocents, InstructionsDocents, \
                    TerritoryContactDocents, SpecialityDocents, CandidateLaborals, \
                    CourseLaborals, InstructionsLaborals, TerritoryContactLaborals, \
                    SpecialityLaborals, TextDocents, TextLaborals, Territory

# Improve Tag administration
class CandidateAdmin(admin.ModelAdmin):
    # List name and category of tags
    list_display = ('dni', 'name', 'surname1', 'surname2', 'is_real_member', 'selected_course', 'assigned_status', 'wait_list')
    list_editable = ('assigned_status',)
    # Allow filtering tags by category
    list_filter = ('selected_course', 'assigned_status')
    ordering = ('wait_list',)
    fields = ('dni', 'name', 'surname1', 'surname2', 'phone', 
              'email', 'region', 'speciality_course',  'selected_course', 'assigned_status', 
              'wait_list', 'is_real_member', 'birthday', 'address', 
              'postal_code', 'city', 'school_name', 'school_city', 'school_code', 
              'current_speciality', 'situation', 'iban', 'territory', 'observations',)   
    def save_model(self, request, obj, form, change):
        try:
            # When there is a change of course, we must reorder waiting lists of
            # affected courses
            # Get previous and new course
            old_course = self.model.objects.get(pk=obj.pk).selected_course
            new_course = obj.selected_course           
            obj.save()
            if old_course != new_course:
                # Reorder waiting lists of previous and new selected courses
                new_course.give_vaccancy_ustec_member()
                new_course.reorder_waitinglist()
                old_course.give_vaccancy_ustec_member()
                old_course.reorder_waitinglist()
        except self.model.DoesNotExist:
            obj.save()

class CourseAdmin(admin.ModelAdmin):
    readonly_fields = ('free_vaccancies',)
    list_display = ('code', 'vaccancies', 'free_vaccancies', 'closed')
    list_editable = ('closed',)
    actions = ['reorder_waitinglist']

    def free_vaccancies(self, instance):
        return instance.n_free_vaccancies()
    free_vaccancies.short_description = "Places lliures"
    
    def reorder_waitinglist(self, request, queryset):
        for c in queryset:
            c.reorder_waitinglist()
    reorder_waitinglist.short_description = "Reordena llistes d'espera dels assessoraments seleccionats"
    
    def save_model(self, request, obj, form, change):
        if change and obj.closed:
            obj.close_vaccancies()
        obj.save()    
    
class MemberAdmin(admin.ModelAdmin):
    list_display = ('dni', 'territory')
    list_filter = ('territory',)
    
class TerritoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'total_courses', 'total_affiliation', 'total')
    def total_courses(self, obj):
        return obj.total_courses()
    total_courses.short_description = "Total aportacions assessoraments"
    def total_affiliation(self, obj):
        return obj.total_affiliation()
    total_affiliation.short_description = "Total pagaments afiliació"
    def total(self, obj):
        return obj.total_courses() + obj.total_affiliation()

admin.site.register(CandidateDocents, CandidateAdmin)
admin.site.register(CandidateLaborals, CandidateAdmin)
admin.site.register(CourseDocents, CourseAdmin)
admin.site.register(CourseLaborals, CourseAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(Territory, TerritoryAdmin)
admin.site.register(TerritoryContactDocents)
admin.site.register(TerritoryContactLaborals)
admin.site.register(InstructionsDocents)
admin.site.register(InstructionsLaborals)
admin.site.register(SpecialityDocents)
admin.site.register(SpecialityLaborals)
admin.site.register(TextDocents)
admin.site.register(TextLaborals)
