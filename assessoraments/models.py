import locale
from django.db import models
from ckeditor.fields import RichTextField
from .utils import send_instructions_mail
from config.models import Config

class Instructions(models.Model):
    code = models.CharField(max_length=20, verbose_name='Codi')
    text_email = models.TextField(blank=True, verbose_name='Text correu electrònic')
    text_web = RichTextField(blank=True, verbose_name='Text web')
    class Meta:
        abstract = True
        verbose_name = 'instrucció'
        verbose_name_plural = 'instruccions'
    def __str__(self):
        return self.code
    
class InstructionsDocents(Instructions):
    class Meta:
        verbose_name = 'instrucció docents'
        verbose_name_plural = 'instruccions docents'

class InstructionsLaborals(Instructions):
    class Meta:
        verbose_name = 'instrucció laborals'
        verbose_name_plural = 'instruccions laborals'

# Create your models here.
class Candidate(models.Model):
    dni = models.CharField(max_length=10, verbose_name='DNI')
    name = models.CharField(max_length=50, verbose_name='Nom')    
    surname1 = models.CharField(max_length=50, verbose_name='Primer Cognom')
    surname2 = models.CharField(max_length=50, blank=True, verbose_name='Segon Cognom')
    phone = models.CharField(max_length=12, verbose_name='Telefon')
    email = models.EmailField(verbose_name='Correu electrònic')
    selected_course = None
    region = models.CharField(max_length=25, blank=True, verbose_name='Comarca')
    speciality_course = None
    observations = models.TextField(blank=True, verbose_name='Observacions')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Data i hora de la inscripció')
    # The mail has been sent correctly
    mail_sent = models.BooleanField(default=False, verbose_name='Correu enviat')
    # The membership has been checked and the candidate is member
    is_real_member = models.BooleanField(default=False, verbose_name='És afiliat')
    # Course assignation status    
    ASSIGNED = 'ASSIGNED'
    WAITLIST = 'WAITLIST'
    REFUSED = 'REFUSED'
    SITUATION_CHOICES = (
        (ASSIGNED, 'Assignat'),
        (WAITLIST, 'Llista d\'espera'),
        (REFUSED , 'Renúncia'),
    )
    assigned_status = models.CharField(default='WAITLIST', max_length=20,
                            choices=SITUATION_CHOICES,
                            verbose_name='Assignació assessorament')
    # If the candidate has no vacancy, set here his/her waitlist order. 0, otherwise
    wait_list = models.IntegerField(default=0, verbose_name="Ordre llista d'espera")
    # Data related to no members that  must be members to be able to do the course
    birthday = models.DateField(blank=True, null=True, verbose_name='Data de naixement')
    address = models.CharField(blank=True, max_length=100, verbose_name='Adreça')
    postal_code = models.CharField(blank=True, max_length=5, verbose_name='Codi Postal')
    city = models.CharField(blank=True, max_length=100, verbose_name='Població')
    school_name = models.CharField(blank=True, max_length=100, verbose_name='Nom del centre')
    school_city = models.CharField(blank=True, max_length=100, verbose_name='Població del centre')
    school_code = models.CharField(blank=True, max_length=8, verbose_name='Codi del centre')
    iban = models.CharField(blank=True, max_length=24, verbose_name='IBAN')
    current_speciality = None
    DEFINITIVE = 'DEFINITIVE'
    PROVISIONAL = 'PROVISIONAL'
    COMISSIO = 'COMISSIO'
    INTERIM = 'INTERIM'
    UNEMPLOYMENT = 'UNEMPLOYMENT'
    ALTRA = 'ALTRA'
    SITUATION_CHOICES = (
        (DEFINITIVE, 'Plaça definitiva'),
        (PROVISIONAL, 'Plaça provisional'),
        (COMISSIO, 'Comissió de serveis'),
        (INTERIM , 'Interinatge o substitucions'),
        (UNEMPLOYMENT, 'Atur'),
        (ALTRA, 'Altra'),
    )
    situation = models.CharField(blank=True, null=True, max_length=20,
                            choices=SITUATION_CHOICES,
                            verbose_name='Situació')
    territory = models.ForeignKey('Territory', on_delete=models.CASCADE, verbose_name='Territori', default=None, blank=True, null=True,)
    InstructionsType = None
    class Meta:
        abstract = True
        verbose_name = 'opositor'
        verbose_name_plural = 'opositors'
    def __str__(self):
        return self.surname1 + " " + self.surname2 + ", " + self.name
    def assign_course(self):
        self.assigned_status = 'ASSIGNED'
        self.wait_list = 0
    def get_instructions(self, text_type="text_email", closed_vaccancies=False):
        if self.assigned_status == 'ASSIGNED':
            if self.is_real_member:
                code = "ASSIGNATAFILIAT"
            else:
                code = "ASSIGNATNOAFILIAT"
        else:
            if not closed_vaccancies:
                code = "LLISTAESPERA"
            else:
                code = "SENSEPLACES"     
        instructions_text = getattr(self.InstructionsType.objects.filter(code=code)[0], text_type)
        instructions_text = self.fill_instructions_template(instructions_text)
        return instructions_text
    
    def fill_instructions_template(self, instructions):
        candidate_course = self.selected_course
        locale.setlocale(locale.LC_ALL, "ca_ES.UTF-8")
        # Replace each tag by its value
        instructions = instructions.replace("{{code}}", 
                                            candidate_course.code) \
                                   .replace("{{place}}", 
                                            candidate_course.place) \
                                   .replace("{{address}}", 
                                            candidate_course.address) \
                                   .replace("{{city}}", 
                                            candidate_course.city) \
                                   .replace("{{dates}}", 
                                            candidate_course.dates) \
                                   .replace("{{territory}}", 
                                            candidate_course.territory_contact.territory.name) \
                                   .replace("{{email}}", 
                                            candidate_course.territory_contact.email)
        return instructions
    
class CandidateDocents(Candidate):
    speciality_course = models.ForeignKey('SpecialityDocents', on_delete=models.CASCADE, related_name='speciality_course', default=None, null=True, verbose_name='Especialitat per a la qual et presentes')
    selected_course = models.ForeignKey('CourseDocents', on_delete=models.CASCADE, verbose_name='Assessorament')
    current_speciality = models.ForeignKey('SpecialityDocents', on_delete=models.CASCADE, related_name='current_speciality', default=None, blank=True, null=True, verbose_name='Especialitat actual')
    InstructionsType = InstructionsDocents
    class Meta:
        verbose_name = 'opositor docent'
        verbose_name_plural = 'opositors docents'

class CandidateLaborals(Candidate):
    speciality_course = models.ForeignKey('SpecialityLaborals', on_delete=models.CASCADE, related_name='speciality_course', default=None, null=True, verbose_name='Categoria professional per a la qual et presentes')
    selected_course = models.ForeignKey('CourseLaborals', on_delete=models.CASCADE, verbose_name='Assessorament')
    current_speciality = models.ForeignKey('SpecialityLaborals', on_delete=models.CASCADE, related_name='current_speciality', default=None, blank=True, null=True, verbose_name='Categoria professional actual')
    InstructionsType = InstructionsLaborals
    class Meta:
        verbose_name = 'opositor laboral'
        verbose_name_plural = 'opositors laborals'
    
class Course(models.Model):
    code = models.CharField(unique=True, max_length=10, verbose_name='Codi')
    speciality = models.CharField(max_length=50, verbose_name="Especialitat")
    territory_contact = None
    city = models.CharField(max_length=50, verbose_name='Població')
    place = models.CharField(max_length=100, verbose_name='Lloc')
    address = models.TextField(verbose_name='Adreça')
    dates = models.TextField(verbose_name='Dates')
    vaccancies = models.IntegerField(default=0, verbose_name='Places')
    closed = models.BooleanField(verbose_name='Assessorament tancat')
    CandidateType = None
    class Meta:
        abstract = True
        verbose_name = 'assessorament'
        verbose_name_plural = 'assessoraments'
        ordering = ['code']
    def __str__(self):
        return self.code
    
    def n_free_vaccancies(self):
        course_students = self.CandidateType.objects.filter(selected_course=self, assigned_status='ASSIGNED')
        free_vaccancies = self.vaccancies - len(course_students)
        return free_vaccancies
    
    def n_waitlist(self):
        waiting_list = self.CandidateType.objects.filter(selected_course=self, 
                                                assigned_status='WAITLIST') \
                                        .exclude(wait_list=-2)
        return len(waiting_list)
    
    def free_vaccancies(self):
        free_vaccancies = self.n_free_vaccancies()
        if free_vaccancies > 0:
            return True
        return False
    
    def reorder_waitinglist(self):
        course_waiting_list_ustec = self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST', is_real_member=True).exclude(wait_list=-2).order_by("date")
        course_waiting_list_no_ustec = self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST', is_real_member=False).exclude(wait_list=-2).order_by("date")
        i = 0
        for i in range(len(course_waiting_list_ustec)):
            course_waiting_list_ustec[i].wait_list = i + 1
            course_waiting_list_ustec[i].save()
        i = len(course_waiting_list_ustec)
        for j in range(len(course_waiting_list_no_ustec)):
            course_waiting_list_no_ustec[j].wait_list = i + 1
            course_waiting_list_no_ustec[j].save()
            i = i + 1
            
    def give_vaccancy_ustec_member(self):
        course_waiting_list_ustec = self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST', is_real_member=True).order_by("date")
        if len(course_waiting_list_ustec) > 0 and self.free_vaccancies():
            course_waiting_list_ustec[0].assign_course()
            course_waiting_list_ustec[0].save()
            
    def close_vaccancies(self):
        self.reorder_waitinglist()
        # Give all vaccancies to the waiting list
        course_waiting_list = self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST').order_by("wait_list")
        while(self.free_vaccancies() and len(course_waiting_list)>0):
            course_waiting_list[0].assign_course()
            course_waiting_list[0].save()
            self.reorder_waitinglist()
            course_waiting_list = self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST').order_by("wait_list")
        # Send a mail to all people who is in waiting list informin they do not have vaccancy
        for c in self.CandidateType.objects.filter(selected_course=self, assigned_status='WAITLIST'):
            instructions = c.get_instructions(closed_vaccancies=True)
            error = send_instructions_mail(c, instructions)
            if error > 0:
                c.mail_sent = True
    
    def close_course(self):
        # Close the course
        self.closed = True
        self.save()
        
    def candidate_list(self):
        candidates = self.CandidateType.objects \
                              .filter(selected_course=self, 
                                      assigned_status="ASSIGNED") \
                              .order_by("surname1", "surname2")
        return candidates
    
class CourseDocents(Course):
    territory_contact = models.ForeignKey('TerritoryContactDocents', on_delete=models.CASCADE, null=True, verbose_name='Territori')
    CandidateType = CandidateDocents
    class Meta:
        verbose_name = 'assessorament docents'
        verbose_name_plural = 'assessoraments docents'
        ordering = ['code']

class CourseLaborals(Course):
    territory_contact = models.ForeignKey('TerritoryContactLaborals', on_delete=models.CASCADE, null=True, verbose_name='Territori')
    CandidateType = CandidateLaborals
    class Meta:
        verbose_name = 'assessorament laborals'
        verbose_name_plural = 'assessoraments laborals'
        ordering = ['code']
        
class Speciality(models.Model):
    code = models.CharField(max_length=30, verbose_name='Especialitat')
    convoked = models.BooleanField(default=False, verbose_name='Especialitat convocada a oposicions')
    class Meta:
        abstract = True
        verbose_name = 'especialitat'
        verbose_name_plural = 'especialitats'
        ordering = ['code']
    def __str__(self):
        return self.code
    
class SpecialityDocents(Speciality):
    class Meta:
        verbose_name = 'especialitat docents'
        verbose_name_plural = 'especialitats docents'
        ordering = ['code']

class SpecialityLaborals(Speciality):
    class Meta:
        verbose_name = 'especialitat laborals'
        verbose_name_plural = 'especialitats laborals'
        ordering = ['code']

class TerritoryContact(models.Model):
    territory = models.ForeignKey('Territory', on_delete=models.CASCADE, verbose_name='Territori')
    email = models.EmailField(verbose_name="Correu electrònic")
    class Meta:
        abstract = True
        verbose_name = 'territori'
        verbose_name_plural = 'territoris'
    def __str__(self):
        return self.territory.name
    
class TerritoryContactDocents(TerritoryContact):
    class Meta:
        verbose_name = 'territori docents'
        verbose_name_plural = 'territoris docents'
    
class TerritoryContactLaborals(TerritoryContact):
    class Meta:
        verbose_name = 'territori laborals'
        verbose_name_plural = 'territoris laborals'

class Member(models.Model):
    dni = models.CharField(max_length=10, verbose_name='DNI')
    territory = models.ForeignKey('Territory', on_delete=models.CASCADE, verbose_name='Territori')
    class Meta:
        verbose_name = 'afiliat'
        verbose_name_plural = 'afiliats'
    def __str__(self):
        return self.dni
    @staticmethod
    def is_member(dni):
        return bool(Member.objects.filter(dni=dni))
    
class Territory(models.Model):
    name = models.CharField(max_length=20, verbose_name='Nom')
    def total_courses(self):
        course_contribution = float(Config.objects.get(key='APORTACIO_ASSESSORAMENT').value)
        return len(CandidateDocents.objects.filter(assigned_status='ASSIGNED', \
                   selected_course__territory_contact__territory=self)) * course_contribution + \
                len(CandidateLaborals.objects.filter(assigned_status='ASSIGNED', \
                   selected_course__territory_contact__territory=self)) * course_contribution
    def total_affiliation(self):
        affiliation_price = float(Config.objects.get(key='PREU_AFILIACIO').value)
        return len(CandidateDocents.objects.filter(assigned_status='ASSIGNED', is_real_member=False, \
                   territory=self)) * affiliation_price + \
               len(CandidateLaborals.objects.filter(assigned_status='ASSIGNED', is_real_member=False, \
                   territory=self)) * affiliation_price
    class Meta:
        verbose_name = 'territori USTEC'
        verbose_name_plural = 'territoris USTEC'
        ordering = ['name']
    def __str__(self):
        return self.name
    
class Text(models.Model):
    key = models.CharField(default="", max_length=50, verbose_name='Clau')
    text = RichTextField(blank=True, verbose_name='Text')
    def __str__(self):
        return self.key
    class Meta:
        abstract = True

class TextDocents(Text):
    class Meta:
        verbose_name = 'text docents'
        verbose_name_plural = 'textos docents'
    
class TextLaborals(Text):
    class Meta:
        verbose_name = 'text laborals'
        verbose_name_plural = 'textos laborals'
    
