from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver
from .models import CandidateDocents, CandidateLaborals
from .utils import send_instructions_mail

@receiver(post_delete, sender=CandidateDocents)
@receiver(post_delete, sender=CandidateLaborals)
def give_vaccancy(sender, instance, *args, **kwargs):
    assigned_status = instance.assigned_status
    course = instance.selected_course
    # if this candidate had a course assigned, give his/her vaccancy 
    # at the first person in waiting list, if any
    if assigned_status == 'ASSIGNED':
        course.give_vaccancy_ustec_member()
    course.reorder_waitinglist()

@receiver(pre_save, sender=CandidateDocents)
@receiver(pre_save, sender=CandidateLaborals)
def organize_course(sender, instance, *args, **kwargs):
    try:
        # candidate is not new
        # Save previous candidate values
        candidate_old = sender.objects.get(pk=instance.pk)
        # Check if it is a course update
        if candidate_old.selected_course != instance.selected_course:
            # If in the new course there are no vaccancies, the person will go to waitlist
            if instance.selected_course.n_free_vaccancies() <= 0:
                instance.assigned_status = 'WAITLIST'
        # If the course has been assigned to candidate, send a mail to candidate
        if candidate_old.assigned_status =='WAITLIST' and  \
            instance.assigned_status == 'ASSIGNED':
            instance.wait_list = 0
            instructions = instance.get_instructions()
            # Send email
            error = send_instructions_mail(instance, instructions)
            if error > 0:
                instance.mail_sent = True
        elif instance.assigned_status == 'REFUSED':
            course = instance.selected_course
            instance.wait_list = 50000
            if candidate_old.assigned_status =='ASSIGNED':
                course.give_vaccancy_ustec_member()
                course.reorder_waitinglist()
    except sender.DoesNotExist:
        # candidate is new
        selected_course = instance.selected_course
        # If the candidate is an USTEC member and there are vaccancies...
        if instance.is_real_member and selected_course.free_vaccancies():
            # Give vaccancy
            instance.assign_course()
        # Check which kind of instructions we have to give
        instructions = instance.get_instructions()
        # Send email
        error = send_instructions_mail(instance, instructions)
        if error > 0:
            instance.mail_sent = True