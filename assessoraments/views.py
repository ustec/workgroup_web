from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt

from .forms import CandidateDocentsStatusForm, CandidateLaboralsStatusForm
from .models import CandidateDocents, CandidateLaborals
from main.utils import get_common_info
from assessoraments.forms import AssessoramentDocentsForm,\
    AssessoramentLaboralsForm, DNIForm
from assessoraments.models import CourseDocents, CourseLaborals, Member, TextDocents, TextLaborals,\
    SpecialityDocents, SpecialityLaborals

@xframe_options_exempt
def get_candidate_data(request, course_type):
    # Get common info of all templates
    data = get_common_info(request)
    # Check what kind of assessoraments we have to deal with
    if course_type == 'docents':
        formType = AssessoramentDocentsForm
        courseType = CourseDocents
        textType = TextDocents
        specialityType = SpecialityDocents
    elif course_type == 'laborals':
        formType = AssessoramentLaboralsForm
        courseType = CourseLaborals
        textType = TextLaborals
        specialityType = SpecialityLaborals
    else:
        raise ValueError("No existeixen aquest tipus d'assessoraments")
    full_form_intro = ''
    # if this is a POST request we'll show the seconf form or process the second form data
    if request.method == 'POST':
        # If 'name' is not received, we show the second form, asking for the rest of data
        if not request.POST.get("name"):
            # Get previous form
            form_dni = DNIForm(request.POST, course_type=course_type)
            # check whether it's valid
            if form_dni.is_valid():
                # Get DNI from the previous form
                dni = form_dni.cleaned_data["dni"] 
                # Fill he DNI of second form with the DNI of previous form 
                # Add also a hidden field informing about if the candidate is member or not 
                form = formType(initial={'dni': dni, 'is_real_member': Member.is_member(dni)})
                form.fields['selected_course'].queryset = courseType.objects.all().filter(closed=False).order_by('code')
                form.fields['speciality_course'].queryset = specialityType.objects.all().filter(convoked=True)
                # Set the form intro text depending on membership
                if Member.is_member(dni):
                    full_form_intro = textType.objects.get(key='INTRO_FORMULARI_AFILIAT').text
                else:
                    #full_form_intro = textType.objects.get(key='INTRO_FORMULARI_NO_AFILIAT').text
                    return render(request, 'assessoraments/not_afiliat_candidate.html', data)
            else:
                # If DNI is not valid, process again first form showing filled data and errors
                form = form_dni
        else:
            # create a seconf form instance and populate it with data from the request
            form = formType(request.POST)
            # check whether it's valid
            if form.is_valid():
                candidate = form.save()
                if candidate.assigned_status != 'ASSIGNED':
                    candidate.selected_course.reorder_waitinglist()
                instructions = candidate.get_instructions('text_web')
                data.update(instructions=instructions)
                return render(request, 'assessoraments/sent.html', data)
    # if a GET (or any other method) we'll the create the DNI blank form
    else:
        form = DNIForm(course_type=course_type)
    title = textType.objects.get(key='TITOL')
    intro = textType.objects.get(key='INTRO')
    footer = textType.objects.get(key='PEU')
    data.update(title=title.text, intro=intro.text, footer=footer.text, full_form_intro=full_form_intro)        
    courses = courseType.objects.all().order_by('code')
    n_courses = courses.count()
    n_closed_courses = courseType.objects.all().filter(closed=True).count()
    all_courses_closed = n_courses == n_closed_courses
    data.update(courses=courses)
    data.update(form=form)
    data.update(course_type=course_type)
    data.update(all_courses_closed=all_courses_closed)
    return render(request, 'assessoraments/index.html', data)

def get_candidate_status(request, course_type):
    # Get common info of all templates
    data = get_common_info(request)
    if course_type == 'docents':
        formType = CandidateDocentsStatusForm
        candidateType = CandidateDocents
    elif course_type == 'laborals':
        formType = CandidateLaboralsStatusForm
        candidateType = CandidateLaborals
    else:
        raise ValueError("No existeixen aquest tipus d'assessoraments")
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = formType(request.POST)
        # check whether it's valid:
        if form.is_valid():
            dni = form.cleaned_data['dni']
            email = form.cleaned_data['email']
            selected_course = form.cleaned_data['selected_course']
            candidate = candidateType.objects.filter(dni=dni) \
                                                                             .filter(email=email) \
                                                                             .filter(selected_course=selected_course) \
                                                                             .get()
            data.update(candidate=candidate)    
            return render(request, 'assessoraments/candidate_status.html', data)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = formType()
    data.update(form=form)
    data.update(course_type=course_type)
    return render(request, 'assessoraments/candidate_status_form.html', data)

@login_required
def courses_lists(request, course_type):
    # Get common info of all templates
    data = get_common_info(request)
    course_pk = request.GET.get('course', '')
    list_type = request.GET.get('type', '')
    if course_type == 'docents':
        courseType = CourseDocents
    elif course_type == 'laborals':
        courseType = CourseLaborals
    else:
        raise ValueError("No existeixen aquest tipus d'assessoraments")
    if course_pk != '':
        course = courseType.objects.all().filter(pk=course_pk)[0]
        candidates = course.candidate_list()
        data.update(code=course.code)
        data.update(candidates=candidates)
        if list_type ==  'attendance':              
            return render(request, 'assessoraments/course_list_detail.html', data)
        elif list_type == 'email':
            return render(request, 'assessoraments/course_list_email.html', data)
            
    courses = courseType.objects.all().order_by('code')
    data.update(courses=courses)
    data.update(course_type=course_type)
    return render(request, 'assessoraments/courses_lists.html', data)
