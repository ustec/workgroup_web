from django.apps import AppConfig


class AssessoramentsConfig(AppConfig):
    name = 'assessoraments'
    verbose_name = 'Assessoraments'
    def ready(self):
        import assessoraments.signals