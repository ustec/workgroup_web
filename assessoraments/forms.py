import re
import stdnum

from django import forms

from .models import CandidateDocents, CandidateLaborals, Member

class DNIForm(forms.Form):
    def __init__(self,*args,**kwargs):
        self.course_type = kwargs.pop('course_type')
        super(DNIForm,self).__init__(*args,**kwargs)
    
    dni = forms.CharField(max_length=9, label="DNI")
    
    def clean_dni(self):
        dni_data = self.cleaned_data
        dni = dni_data.get('dni')
        dni = dni.replace('-','').replace(' ','') .upper()
        if not self.valid_dni(dni):
            raise forms.ValidationError("El DNI ha de tenir el format 12345678A o  X7962401A i ser vàlid.")
        if self.duplicated_dni(dni):
            raise forms.ValidationError("Aquest DNI ja ha fet una sol·licitud.")
        return dni
    
    @staticmethod
    def valid_dni(dni):
        mod_dni = stdnum.get_cc_module('es', 'dni')
        mod_nie = stdnum.get_cc_module('es', 'nie')
        return mod_dni.is_valid(dni) or mod_nie.is_valid(dni)
    
    def duplicated_dni(self, dni):
        CandidateType = None
        if self.course_type == "docents":
            CandidateType = CandidateDocents
        else:
            CandidateType = CandidateLaborals
        return len(CandidateType.objects.filter(dni=dni)) != 0    

class AssessoramentForm(forms.ModelForm):
    CandidateType = None
    
    class Meta:
        abstract = True
        fields = ['dni', 'name', 'surname1', 'surname2', 'phone', 'email', 'region', 
                  'speciality_course', 'selected_course', 'birthday', 'address', 
                  'postal_code', 'city', 'school_name', 'school_city', 'school_code', 'current_speciality', 
                  'situation', 'iban', 
                  'observations', 'is_real_member']
        labels = {
            'dni': 'NIF/NIE',
            'name': 'Nom',
            'surname1': 'Primer Cognom',
            'surname2': 'Segon Cognom',
            'phone': 'Telèfon',
            'email': 'Correu electrònic',  
            'region': 'Comarca on vius',      
            'observations': 'Observacions',
            'iban': 'Número de compte'        
        }
        help_texts = {
            'iban': 'El número de compte s\'ha d\'escriure complet i sense espais. Exemple de número de compte: ES2112341234121234567890'
        }
        widgets = {
            'dni': forms.TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'surname1': forms.TextInput(attrs={'class': 'form-control'}),
            'surname2': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'region': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}), 
            'observations': forms.Textarea(attrs={'class': 'form-control'}),
            'birthday': forms.SelectDateWidget(years=range(1950, 2000),
                                                months={1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9, 10:10, 11:11, 12:12}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'postal_code': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'school_name': forms.TextInput(attrs={'class': 'form-control'}),
            'school_city': forms.TextInput(attrs={'class': 'form-control'}),
            'school_code': forms.TextInput(attrs={'class': 'form-control'}),
            'iban': forms.TextInput(attrs={'class': 'form-control'}),
            'is_real_member': forms.HiddenInput()
        }
    
    # Validators
    
    def clean_phone(self):
        candidate = self.cleaned_data
        phone = candidate.get('phone')
        if not self.valid_phone(phone):
            raise forms.ValidationError("El número de telefon han de ser 9 dígits sense espais. Exemple: 666555444")
        return phone
    
    def clean_birthday(self):
        req_field = self.required_field('birthday', "La data de naixement és obligatòria." )  
        return req_field
    
    def clean_address(self):
        req_field = self.required_field('address', "L'adreça és obligatòria." )  
        return req_field
    
    def clean_postal_code(self):
        req_field = self.required_field('postal_code', "El codi postal és obligatori." )  
        return req_field
    
    def clean_iban(self):
        candidate = self.cleaned_data
        iban = candidate.get('iban')
        # If user is member IBAN is required
        self.required_field('iban', "L'IBAN és obligatori." )        
        # IBAN validation
        mod_iban = stdnum.get_cc_module('es', 'iban')
        if iban != '' and not mod_iban.is_valid(iban):
            raise forms.ValidationError("L'IBAN no és vàlid.")
        return iban
     
    def required_field(self, field, error):
        candidate = self.cleaned_data
        req_field = candidate.get(field)
        # If user is member IBAN is required
        dni = candidate.get('dni')
        is_member = Member.is_member(dni)
        if not is_member and (req_field == '' or req_field == None):
            raise forms.ValidationError(error)
        return req_field
    
    @staticmethod
    def valid_phone(phone):
        # phone must be 9 digits
        return True if re.match("^\d{9}$", phone) else False
    
class AssessoramentDocentsForm(AssessoramentForm):
    CandidateType = CandidateDocents
    class Meta(AssessoramentForm.Meta):
        model = CandidateDocents
        
class AssessoramentLaboralsForm(AssessoramentForm):
    CandidateType = CandidateLaborals
    class Meta(AssessoramentForm.Meta):
        model = CandidateLaborals
    
class CandidateStatusForm(forms.ModelForm):    
    CandidateType = None
    class Meta:
        abstract = True
        fields = ['dni', 'email', 'selected_course']
        labels = {
            'dni': 'DNI',
            'email': 'Correu electrònic',        
        }
        widgets = {
            'dni': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
        }
    
    def clean(self):
        cleaned_data = super().clean()
        dni = cleaned_data.get("dni")
        email = cleaned_data.get("email")
        selected_course = cleaned_data.get("selected_course")
        if len(self.CandidateType.objects.filter(dni=dni).filter(email=email).filter(selected_course=selected_course))== 0:
            raise forms.ValidationError(
                    "Aquesta inscripció no existeix."
                )
            
class CandidateDocentsStatusForm(CandidateStatusForm):
    CandidateType = CandidateDocents
    class Meta(CandidateStatusForm.Meta):
        model = CandidateDocents
        
class CandidateLaboralsStatusForm(CandidateStatusForm):
    CandidateType = CandidateLaborals
    class Meta(CandidateStatusForm.Meta):
        model = CandidateLaborals
