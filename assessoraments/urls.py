from django.urls import path
from . import views

urlpatterns = [
    path('<course_type>/candidate_status', views.get_candidate_status, name='get_candidate_status'),
    path('<course_type>/courses_lists', views.courses_lists, name='courses_lists'),
    path('<course_type>/course_list_detail', views.courses_lists, name='course_list_detail'),
    path('<course_type>/course_list_email', views.courses_lists, name='course_list_email'),
    path('<course_type>/', views.get_candidate_data),
]