from django.contrib import admin
from ordered_model.admin import OrderedModelAdmin
from .models import Poster, PosterImage

class PosterImageInline(admin.TabularInline):
    model = PosterImage

# Register your models here.
class PosterAdmin(OrderedModelAdmin):
    date_hierarchy = 'date_time'
    # List name and category of tags
    list_display = ('title', 'move_up_down_links', 'visible')
    list_filter = ('visible',)
    inlines = [PosterImageInline]
    
admin.site.register(Poster, PosterAdmin)