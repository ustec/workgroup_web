from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from ordered_model.models import OrderedModel

common_media_storage = FileSystemStorage(location=settings.COMMON_MEDIA_ROOT, \
                                         base_url=settings.COMMON_MEDIA_URL)

# Posters showed in main page
class Poster(OrderedModel):
    title = models.CharField(max_length=100, verbose_name='Títol')
    date_time = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(default=True)
    class Meta(OrderedModel.Meta):
        verbose_name = 'cartell'
        verbose_name_plural = 'cartells'
    def __str__(self):
        return self.title

# A poster can have more than one image. If it has more than one image it will
# be a carousel.
class PosterImage(models.Model):
    alt = models.CharField(max_length=100, verbose_name='Text alternatiu', \
                           blank=True, null=True,)
    image = models.ImageField(storage=common_media_storage, \
                              upload_to='posters/', \
                              verbose_name='Imatge del cartell')
    link = models.CharField(max_length=200, \
                            blank=True, null=True,
                            verbose_name='Enllaç')
    poster = models.ForeignKey('Poster', verbose_name='Cartell', \
                               on_delete=models.CASCADE,)
    landscape_mode = models.BooleanField(default=False, verbose_name='Posar la imatge apaisada')