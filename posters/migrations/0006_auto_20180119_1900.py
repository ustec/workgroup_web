# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-19 18:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posters', '0005_auto_20170510_1239'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='poster',
            options={'ordering': ('order',), 'verbose_name': 'cartell', 'verbose_name_plural': 'cartells'},
        ),
        migrations.AlterField(
            model_name='poster',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
    ]
