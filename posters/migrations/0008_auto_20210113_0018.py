# Generated by Django 2.2.17 on 2021-01-12 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posters', '0007_poster_visible'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poster',
            name='order',
            field=models.PositiveIntegerField(db_index=True, editable=False, verbose_name='order'),
        ),
    ]
