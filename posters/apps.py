from django.apps import AppConfig


class PostersConfig(AppConfig):
    name = 'posters'
    verbose_name = 'Cartells'
    def ready(self):
        import posters.signals