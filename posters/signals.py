from django.db.models.signals import pre_save, post_delete, post_save
from django.dispatch import receiver
from .models import Poster, PosterImage
import os.path

# If a poster is removed, remove associated file as well
@receiver(post_delete, sender=PosterImage)
def delete_file(sender, instance, *args, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)

# If a poster image has no alt attribute, fill it with its corresponding poster
# title
@receiver(pre_save, sender=PosterImage)
def fill_alt(sender, instance, *args, **kwargs):
    if instance.alt == '':
        instance.alt = instance.poster.title
        
@receiver(pre_save, sender=Poster)
def reorder_poster(sender, instance, update_fields, *args, **kwargs):
    # Get poster before being saved
    try:
        poster_old = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        poster_old = None
    # Disconnect this method from presave to avoid infinite loop saving
    # top and bottom methods call save method
    pre_save.disconnect(reorder_poster, sender=Poster)
    # Move poster if visibility has changed or if it is a new poster
    if poster_old and poster_old.visible != instance.visible or poster_old is None:
        if instance.visible:
            instance.top()
        else:
            instance.bottom()
    # Connect to presave again
    pre_save.connect(reorder_poster, sender=Poster)