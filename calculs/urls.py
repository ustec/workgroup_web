from django.conf.urls import url
from . import views

urlpatterns = [    
    url(r'^$', views.index),
    url(r'^increments_retributius_2018$', views.salary_increases_2018, name='increments_retributius_2018'),
    url(r'^paga_extra_2013_30$', views.extra_paycheck_2013_30, name='paga_extra_2013_30'),
    url(r'^paga_extra_2013_60$', views.extra_paycheck_2013_60, name='paga_extra_2013_60'),
] 