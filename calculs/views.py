from django.shortcuts import render
from main.utils import get_common_info

def index(request):
    context = get_common_info(request)
    return render(request, 'calculs/index.html', context)

def salary_increases_2018(request):
    context = get_common_info(request)
    return render(request, 'calculs/increments_retributius_2018.html', context)

def extra_paycheck_2013_30(request):
    context = get_common_info(request)
    return render(request, 'calculs/paga_extra_2013_30.html', context)

def extra_paycheck_2013_60(request):
    context = get_common_info(request)
    return render(request, 'calculs/paga_extra_2013_60.html', context)
