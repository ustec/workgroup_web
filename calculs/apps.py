from django.apps import AppConfig


class CalculsConfig(AppConfig):
    name = 'calculs'
