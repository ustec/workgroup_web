from django import forms

from .models import AfiliationModel

class AfiliationForm(forms.ModelForm):
    class Meta:
        model = AfiliationModel
        fields = [
            'dni',
            'nom',
            'primerCognom',
            'segonCognom', 
            'sexe',
            'codiCentre',
            'institut',
            'laboralDocent',
            'codiEspecialitat',
            'telefon1',
            'correu1',
            'adreca',
            'codiPostal',
            'territoriUstec',
            'dataNaixement'
        ]

        # widget = {
        #     'title': forms.TextInput
        # }