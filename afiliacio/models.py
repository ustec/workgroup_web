from django.db import models
from django import forms
# Create your models here.
class AfiliationModel(models.Model):
    dni = models.CharField(max_length=9, default="")
    nom = models.CharField(max_length=20, default="")
    primerCognom = models.CharField(max_length=20, default="")
    segonCognom = models.CharField(max_length=20, default="")
    sexe = models.CharField(max_length=20,choices=[('Home', 'H'), ('Dona', 'D')], default='Home')
    codiCentre = models.IntegerField(default=0)
    institut = models.CharField(max_length=20,choices=[('Guineueta', 'Guine'), ('Puig Castellar', 'Puig')], default='Guineueta')
    laboralDocent = models.CharField(max_length=20, choices=[('Laboral', 'Lab'), ('Docent', 'Doc')], default='Docent')
    codiEspecialitat = models.IntegerField(default=0)
    telefon1 = models.IntegerField(default=0)
    correu1 = models.EmailField(default="")
    adreca = models.CharField(max_length=40, default="")
    # codiPoblacio = models.IntegerField()
    codiPostal = models.IntegerField(default=0)
    territoriUstec = models.CharField(default='Barcelona', max_length=20, choices=[('Barcelona', 'bcn'), ('Tarragona', 'trg'), ('Lleida', 'lle'), ('Girona', 'grn')])
    dataNaixement = models.DateField(default='2021/1/1')
    # IBAN = models.CharField()
    # entitat = models.IntegerField()
    # oficina = models.IntegerField()
    # codiControl = models.IntegerField()
    # numCompte = models.IntegerField()