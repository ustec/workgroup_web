from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from .forms import AfiliationForm
from .models import AfiliationModel

def afiliacio (request):
    
    form= AfiliationForm(request.POST or None)
    if form.is_valid():
        dni = form.cleaned_data['dni']
        nom = form.cleaned_data['nom']
        primerCognom = form.cleaned_data['primerCognom']
        segonCognom = form.cleaned_data['segonCognom']
        sexe = form.cleaned_data['sexe']
        codiCentre = form.cleaned_data['codiCentre']
        institut = form.cleaned_data['institut']
        laboralDocent = form.cleaned_data['laboralDocent']
        codiEspecialitat = form.cleaned_data['codiEspecialitat']
        telefon1 = form.cleaned_data['telefon1']
        correu1 = form.cleaned_data['correu1']
        adreca = form.cleaned_data['adreca']
        codiPostal = form.cleaned_data['codiPostal']
        territoriUstec = form.cleaned_data['territoriUstec']
        dataNaixement = form.cleaned_data['dataNaixement']
        form.save()
        # nom = form.cleaned_data['name']
        # mail =  form.cleaned_data['email']
        # inst = form.cleaned_data ['institut']

        # print (nom, mail, inst ) 
    context = {
        'form' : form
    }
    
    return render(request, 'form.html', context)

def helloWorld (request):

    return HttpResponse ("Hello world")