from datetime import datetime
from django.db.models import Max
from django.shortcuts import render
from main.utils import get_common_info
from departament.models import Cos, ServeiTerritorial
from especialitat_borsa.models import EspecialitatBorsa

def index(request):
    context = get_common_info(request)
    # User selected date or last date in database by default 
    try:
        date_form = request.GET['data']
        selected_date = datetime.strptime(date_form, '%Y-%m-%d')
    except:
        selected_date = EspecialitatBorsa.objects.aggregate(Max('data'))['data__max']
    context.update(selected_date=selected_date)
    # Get all dates
    dates = EspecialitatBorsa.objects.values('data').distinct().order_by('-data')
    context.update(dates=dates)
    # Get all cossos, except "others"
    cossos = Cos.objects.exclude(id_cos='999')
    context.update(cossos=cossos)
    # Get all st
    sts = ServeiTerritorial.objects.all()
    context.update(sts=sts)
    especialitats_convocades = {}
    # For each cos get a dict with especialitats convocades
    for cos in cossos: 
        especialitats_convocades[cos] = EspecialitatBorsa.get_especialitat_st_dict(selected_date, cos.id_cos)
    context.update(especialitats_convocades=especialitats_convocades)
    # For each cos and date, list especialitats
    return render(request, 'especialitat_borsa/index.html', context)