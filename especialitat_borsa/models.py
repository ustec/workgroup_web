from collections import OrderedDict
from django.db import models
from departament.models import ServeiTerritorial

class EspecialitatBorsa(models.Model):
    data = models.DateField(verbose_name='Data')
    id_st_dept = models.ForeignKey('departament.ServeiTerritorial', models.DO_NOTHING, db_column='id_st_dept', to_field='id_st_dept', verbose_name='Servei territorial')
    id_cos = models.ForeignKey('departament.Cos', models.DO_NOTHING, db_column='id_cos', to_field='id_cos', verbose_name='Cos')
    id_espe = models.ForeignKey('departament.Especialitat', models.DO_NOTHING, db_column='id_espe', to_field='id_espe', verbose_name='Especialitat')
    
    @staticmethod
    def get_especialitat_st_dict(day, cos):
        esp_st_dict = OrderedDict()
        # Get distinct espacilitats thar are called today
        esps = EspecialitatBorsa.objects.filter(data=day).filter(id_cos=cos).values('id_espe').distinct().order_by('id_espe')
        # Get all serveis territorials
        sts = ServeiTerritorial.objects.all()
        for e in esps:
            id_espe = e['id_espe']
            esp_st_dict[id_espe] = {}
            # Get if esp is called for each servei territorial
            for st in sts:
                c= EspecialitatBorsa.objects.filter(data=day).filter(id_cos=cos).filter(id_st_dept=st.id_st_dept).filter(id_espe=id_espe)
                if c:
                    esp_st_dict[id_espe][st.id] = True
                else:
                    esp_st_dict[id_espe][st.id] = False
        return esp_st_dict
    
    class Meta:
        db_table = 'especialitat_borsa'
        verbose_name = 'especialitat borsa'
        verbose_name_plural = 'especialitats borsa'