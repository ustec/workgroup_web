from django.apps import AppConfig


class EspecialitatBorsaConfig(AppConfig):
    name = 'especialitat_borsa'
