from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import Video
        
@receiver(pre_save, sender=Video)
def reorder_video(sender, instance, update_fields, *args, **kwargs):
    # Get poster before being saved
    try:
        video_old = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        video_old = None
    # Disconnect this method from presave to avoid infinite loop saving
    # top and bottom methods call save method
    pre_save.disconnect(reorder_video, sender=Video)
    # Move poster if visibility has changed or if it is a new poster
    if video_old and video_old.visible != instance.visible or video_old is None:
        if instance.visible:
            instance.top()
        else:
            instance.bottom()
    # Connect to presave again
    pre_save.connect(reorder_video, sender=Video)