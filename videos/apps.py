from django.apps import AppConfig


class VideosConfig(AppConfig):
    name = 'videos'
    verbose_name = 'Vídeos'
    def ready(self):
        import videos.signals