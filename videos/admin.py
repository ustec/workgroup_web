from django.contrib import admin
from ordered_model.admin import OrderedModelAdmin
from .models import Video

class VideoAdmin(OrderedModelAdmin):
    date_hierarchy = 'date_time'
    # List name and category of tags
    #list_display = ('title', 'order')
    list_display = ('title', 'move_up_down_links', 'visible')
    list_editable = ('visible',)
    list_filter = ('visible', 'featured',)
    ordering = ('-featured','order')
    class Media:
        js = ("js/admin_video.js",)
    
admin.site.register(Video, VideoAdmin)