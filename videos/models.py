from django.db import models
from ordered_model.models import OrderedModel

# Videos showed in main page
class Video(OrderedModel):
    title = models.CharField(max_length=100, verbose_name='Títol')
    date_time = models.DateTimeField(auto_now_add=True)
    PEERTUBE = 'PEERTUBE'
    YOUTUBE = 'YOUTUBE'
    VIMEO = 'VIMEO'
    URL = 'URL'
    ITEM_TYPE_CHOICES = (
        (PEERTUBE, 'Peertube'),
        (YOUTUBE, 'Youtube'),
        (VIMEO, 'Vimeo'),
        (URL, 'URL'),
    )
    type = models.CharField(max_length=100,
                            choices=ITEM_TYPE_CHOICES, 
                            default=PEERTUBE, 
                            verbose_name='Tipus de vídeo')
    video_id = models.CharField(blank=True, max_length=50, verbose_name='Identificador del vídeo')
    url = models.URLField(blank=True)
    visible = models.BooleanField(default=True, verbose_name='Visible')
    featured = models.BooleanField(default=False, verbose_name='Destacat')
    class Meta(OrderedModel.Meta):
        verbose_name = 'vídeo'
        verbose_name_plural = 'vídeos'
    def __str__(self):
        return self.title