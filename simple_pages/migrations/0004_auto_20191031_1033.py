# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-10-31 09:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simple_pages', '0003_remove_tag_short_name'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tag',
            options={'ordering': ['name'], 'verbose_name': 'etiqueta', 'verbose_name_plural': 'etiquetes'},
        ),
    ]
