from django.apps import AppConfig


class SimplePagesConfig(AppConfig):
    name = 'simple_pages'
    verbose_name = 'Pàgines'
