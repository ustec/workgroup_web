from django.shortcuts import get_object_or_404, render

from main.utils import get_common_info
from .models import Page

def page(request, short_name):
    # Get common info of all templates
    context = get_common_info(request)
    page = get_object_or_404(Page, short_name=short_name)
    # Return data to the template
    context.update(page=page)
    return render(request, 'simple_pages/page.html', context)