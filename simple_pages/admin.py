from django.contrib import admin
from .models import Page, Tag

class PageAdmin(admin.ModelAdmin):
    ordering = ('name',)
    # Improve tag selection
    filter_horizontal = ('tags',)
    list_filter = ('tags',)
    
class TagAdmin(admin.ModelAdmin):
    ordering = ('name',)

admin.site.register(Page, PageAdmin)
admin.site.register(Tag, TagAdmin)
