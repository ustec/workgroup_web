from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nom')
    class Meta:
        verbose_name = 'etiqueta'
        verbose_name_plural = 'etiquetes'
        ordering = ['name']
    def __str__(self):
        return self.name

class Page(models.Model):
    short_name = models.CharField(max_length=50, verbose_name='Nom curt')
    name = models.CharField(max_length=100, verbose_name='Nom')
    body = RichTextUploadingField(blank=True, verbose_name='Cos')
    tags = models.ManyToManyField(Tag, verbose_name='Etiquetes')
    class Meta:
        verbose_name = 'pàgina'
        verbose_name_plural = 'pàgines'
    def __str__(self):
        return self.name