from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<short_name>[0-9a-z\-]+)/$', views.page),
]