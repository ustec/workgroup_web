"""ustecfp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from django.apps import apps

handler404 = 'main.views.redirect_old_web'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('m/', include('main_menu.urls')),
    path('', include('main.urls'))
    # , path('afiliacio/', include('afiliacio.urls'))
]

if apps.is_installed('assessoraments'):
    urlpatterns.insert(0, path('assessoraments/', include('assessoraments.urls')))

if apps.is_installed('calculs'):
    urlpatterns.insert(0, path('calculs/', include('calculs.urls')))

if apps.is_installed('especialitat_borsa'):
    urlpatterns.insert(0, path('especialitat_borsa/', include('especialitat_borsa.urls')))

if apps.is_installed('dades_plantilles'):
    urlpatterns.insert(0, path('dades_plantilles/', include('dades_plantilles.urls')))

if apps.is_installed('simple_pages'):
    urlpatterns.insert(0, path('p/', include('simple_pages.urls')))

if apps.is_installed('afiliacio'):
    urlpatterns.insert(0, path('afiliacio/', include('afiliacio.urls')))
