"""
WSGI config for eleccions19 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "workgroup.settings.prod_eleccions19")
os.environ.setdefault("DJANGO_SECRET_FILE", "/etc/workgroup_web/eleccions19_secret")
os.environ.setdefault("DJANGO_COMMON_SECRET_FILE", "/etc/workgroup_web/common_secret")

# Activate virtual env
activate_env=os.path.expanduser("/var/www/workgroup_env/bin/activate_this.py")
with open(activate_env) as f:
    code = compile(f.read(), activate_env, 'exec')
    exec(code, dict(__file__=activate_env))


application = get_wsgi_application()
