import configparser
from workgroup.settings.prod_base import *

# SECURITY WARNING: keep the secret key used in production secret!
secrets = configparser.ConfigParser()
secrets.read(os.environ['DJANGO_SECRET_FILE'])

SECRET_KEY = secrets.get('Django', 'SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

WSGI_APPLICATION = 'workgroup.wsgi_prod_fpa.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': secrets.get('Database', 'NAME'),
        'USER': secrets.get('Database', 'USER'),
        'PASSWORD': secrets.get('Database', 'PASSWORD'),
        'HOST': 'localhost',
        'PORT': '3306'
    }
})

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/var/www/workgroup_static'
MEDIA_URL = '/media/fpa/'
MEDIA_ROOT = '/var/www/workgroup_media/fpa'

# Site configuration

WORKGROUP_ID = '3'
WORKGROUP_NAME = 'Educació Persones Adultes'
WORKGROUP_MAIL = 'fpa@sindicat.net'
WORKGROUP_DESCRIPTION = 'Sindicat de l\'Ensenyament  de Catalunya USTEC STEs (IAC) - Unio Sindical de Treballadors/es de l\'Ensenyament de Catalunya - Espai dedicat l\'Educació de les Persones Adlutes'
