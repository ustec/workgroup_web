from workgroup.settings.dev_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+n_8upc$t5jx9nnq!9wf=^uu=(wp%%3e8z0(1&ivc=c67x7pti'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost']

WSGI_APPLICATION = 'workgroup.wsgi_dev.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wg_fp',
        'USER': 'wg_fp',
        'PASSWORD': 'ustecfp',
        'HOST': 'localhost',
        'PORT': '3306'
    }
})

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/fp/'
MEDIA_ROOT = './media/fp/'

# Site configuration

WORKGROUP_ID = '2'
WORKGROUP_NAME = 'Formació Professional'
WORKGROUP_MAIL = 'fp@sindicat.net'
WORKGROUP_DESCRIPTION = 'Sindicat de l\'Ensenyament  de Catalunya USTEC STEs (IAC) - Unio Sindical de Treballadors/es de l\'Ensenyament de Catalunya - Espai dedicat la Formació Professional i Ensenyaments de Règim Especial'
