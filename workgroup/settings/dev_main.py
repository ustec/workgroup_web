from workgroup.settings.dev_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+n_8upc$t5jx9nnq!9wf=^uu=(wp%%3e8z0(1&ivc=c67x7pti'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

WSGI_APPLICATION = 'workgroup.wsgi_dev.application'

INSTALLED_APPS = INSTALLED_APPS + ('assessoraments',
                                   'simple_pages',
                                   'departament',
                                   'nomenaments',
                                   'afiliacio',
                                   'calculs',
                                   'dades_plantilles',
                                   'especialitat_borsa'
                                   )

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'main',
        'USER': 'main',
        'PASSWORD': 'main',
        'HOST': 'localhost',
        'PORT': '3306'
    },
    'departament': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'departament',
        'USER': 'departament',
        'PASSWORD': 'departament',
        'HOST': 'localhost',
        'PORT': '3306'
    },
    'assessoraments': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'assessoraments',
        'USER': 'assessoraments',
        'PASSWORD': 'assessoraments',
        'HOST': 'localhost',
        'PORT': '3306'
    },
})

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/main/'
MEDIA_ROOT = './media/main/'

# Site configuration

WORKGROUP_ID = '1'
WORKGROUP_NAME = ''
WORKGROUP_MAIL = 'fp@sindicat.net'
WORKGROUP_DESCRIPTION = 'Sindicat de l\'Ensenyament  de Catalunya USTEC STEs (IAC) - Unio Sindical de Treballadors/es de l\'Ensenyament de Catalunya'

LOGIN_URL = "/admin/login"

OLD_WEB = "http://antiga.sindicat.net"
