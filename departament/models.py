from django.db import models

class ServeiTerritorial(models.Model):
    id_st_ustec = models.IntegerField(unique=True, verbose_name='Codi USTEC')
    id_st_dept = models.CharField(unique=True, max_length=4, verbose_name='Codi Departament')
    nom = models.CharField(max_length=100, blank=True, null=True, verbose_name='Nom')
    nom_curt = models.CharField(max_length=100, blank=True, null=True, verbose_name='Nom curt')
    class Meta:
        db_table = 'servei_territorial'
        verbose_name = 'servei territorial'
        verbose_name_plural = 'serveis territorials'
        
class Cos(models.Model):
    id_cos = models.CharField(unique=True, max_length=5, verbose_name='Codi Departament')
    nom = models.CharField(max_length=50, blank=True, null=True, verbose_name='Nom')
    class Meta:
        db_table = 'cos'
        verbose_name = 'cos'
        verbose_name_plural = 'cossos'
        
class Especialitat(models.Model):
    id_espe = models.CharField(unique=True, max_length=5, verbose_name='Codi')
    id_cos = models.ForeignKey('Cos', models.DO_NOTHING, db_column='id_cos', to_field='id_cos', verbose_name='Cos')
    class Meta:
        db_table = 'especialitat'
        verbose_name = 'especialitat'
        verbose_name_plural = 'especialitats'
        
class EspecialitatNom(models.Model):
    id_espe = models.ForeignKey('Especialitat', models.DO_NOTHING, db_column='id_espe', to_field='id_espe', verbose_name='Codi')
    nom = models.CharField(max_length=255, blank=True, null=True, verbose_name='Nom')
    class Meta:
        db_table = 'especialitat_nom'
        verbose_name = 'nom especialitat'
        verbose_name_plural = 'noms especialitats'