from django.apps import AppConfig


class NomenamentsConfig(AppConfig):
    name = 'nomenaments'
