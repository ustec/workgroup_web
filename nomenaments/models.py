from __future__ import unicode_literals

from django.db import models
from datetime import datetime
from collections import OrderedDict
from departament.models import ServeiTerritorial

class NomenamentNumero(models.Model):
    data = models.DateField(verbose_name='Data')
    id_st_dept = models.ForeignKey('departament.ServeiTerritorial', models.DO_NOTHING, db_column='id_st_dept', to_field='id_st_dept', verbose_name='Servei territorial')
    nivell = models.CharField(max_length=2, verbose_name='Nivell')
    id_espe = models.ForeignKey('departament.Especialitat', models.DO_NOTHING, db_column='id_espe', to_field='id_espe', verbose_name='Especialitat')
    numero_borsa = models.IntegerField(verbose_name='Número de borsa')
    
    @staticmethod
    def get_nomenaments_num_dict(day=datetime.now().date()):
        nomenaments_num_dict = OrderedDict()
        # Get distinct espacilitats thar are called today
        esps = NomenamentNumero.objects.filter(data=day).values('id_espe').distinct().order_by('nivell', 'id_espe')
        # Get all serveis territorials
        sts = ServeiTerritorial.objects.all()
        for e in esps:
            id_espe = e['id_espe']
            nomenaments_num_dict[id_espe] = {}
            # Get number for each especialitat and servei territorial
            for st in sts:
                n = NomenamentNumero.objects.filter(data=day).filter(id_st_dept=st.id_st_dept).filter(id_espe=id_espe)
                # For each servei territorial and especialitat, create a list
                # This list will have one or two numbers
                # It will have two numbers only when secondary teachers give 
                # lessons in primary school
                nomenaments_num_dict[id_espe][st.id_st_ustec] = []
                if n:
                    nomenaments_num_dict[id_espe][st.id_st_ustec].append(n[0].numero_borsa)
                    if len(n) > 1:
                        nomenaments_num_dict[id_espe][st.id_st_ustec].append(n[1].numero_borsa)
                else:
                    nomenaments_num_dict[id_espe][st.id_st_ustec].append(0)
        return nomenaments_num_dict

    class Meta:
        db_table = 'nomenament_numero'
        unique_together = (('data', 'id_st_dept', 'nivell', 'id_espe'),)
        verbose_name = 'número nomenat'
        verbose_name_plural = 'números nomenats'