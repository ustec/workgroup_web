from collections import OrderedDict
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404
from .models import Entry, Category, Tag
from main_menu.models import Item
if apps.is_installed('simple_pages' ):
    from simple_pages.models import Page
from posters.models import Poster
from videos.models import Video
from main_menu.utils import get_main_menu, get_external_links

import requests

def get_workgroup_menu():
    # Use an OrderedDict, so items will be presented in he order they are added to the dict
    workgroup_menu = OrderedDict()
    # Get all categories
    categories = Category.objects.filter(visible=True)
    # For each category get all its tags
    for category in categories:
        workgroup_menu[category] = Tag.objects.filter(category=category.id)
    return workgroup_menu

# Get common info present in all templates
def get_common_info(request):
    # Get workgrup info from settings
    workgroup_id = settings.WORKGROUP_ID
    workgroup_name = settings.WORKGROUP_NAME
    workgroup_mail = settings.WORKGROUP_MAIL
    workgroup_description = settings.WORKGROUP_DESCRIPTION
    if hasattr(settings, 'SHOW_MENU'):
        show_menu = settings.SHOW_MENU
    else:
        show_menu = True
    # Get main workgroup_menu
    main_menu = ''
    external_links=''
    if show_menu:
        main_menu = get_main_menu()
        external_links = get_external_links()
    # Get workgroup workgroup_menu
    workgroup_menu = get_workgroup_menu()
    # Get breadcrumb
    breadcrumb = get_breadcrumb(request.path, workgroup_name)
    # Get media
    posters = Poster.objects.filter(visible=True).order_by('order')
    featured_videos = Video.objects.filter(visible=True).filter(featured=True).order_by('order')
    videos = Video.objects.filter(visible=True).filter(featured=False).order_by('order')
    visits = get_visits(request)
    return {'workgroup_id': workgroup_id,
            'workgroup_name': workgroup_name,
            'workgroup_mail': workgroup_mail,
            'workgroup_description': workgroup_description,
            'show_menu': show_menu,
            'main_menu': main_menu,
            'external_links': external_links,
            'workgroup_menu': workgroup_menu,
            'breadcrumb': breadcrumb,
            'posters': posters,
            'featured_videos': featured_videos,
            'videos': videos,
            'visits': visits}

# Get the corresponding entries of a given page
def get_entries_per_page(page, all_entries):
    if apps.is_installed('config' ):
        Config = apps.get_model('config', 'Config')
        entrades = Config.objects.filter(key='ENTRADES_PER_PAGINA').first()
        if entrades is not None and entrades.value != '' and entrades.value !='0':
            #if ENTRADES_PER_PAGINA exists and is not empty
            paginator = Paginator(all_entries, entrades.value)
        else:
            #default value for ENTRADES_PER_PAGINA if is not set in configuració
            paginator = Paginator(all_entries, 40)
    try:
        entries = paginator.page(page)
    except PageNotAnInteger:
        entries = paginator.page(1)
    except EmptyPage:
        entries = paginator.page(paginator.num_pages)
    return entries

def get_breadcrumb(path, workgroup_name):
    section = path.split('/')[1]
    breadcrumb = [] 
    # If it is the workgroup main page, add minimal breadcrumb: home and workgroup name   
    if (section == ''  and workgroup_name != ''):
        breadcrumb.append(['Inici', 'http://sindicat.net'])
        breadcrumb.append([workgroup_name, '/'])
    # If it is not the main page, build breadcrumb
    if (section != ''):
        breadcrumb.append(['Inici', 'http://sindicat.net'])
        if (workgroup_name != ''):
            breadcrumb.append([workgroup_name, '/'])
        # Get what kind of page are we looking:
        # c -> entries of a category
        # t -> entries of a tag
        # e -> entry
        # p -> simple page
        # m -> main submenu
        # Get identifier and look for it in its corresponding model
        identifier = path.split('/')[2]
        if (section == 'c'):
            c = get_object_or_404(Category, short_name=identifier)
            breadcrumb.append([c.name, '/c/' + c.short_name])
        elif (section == 't'):
            t = get_object_or_404(Tag, short_name=identifier)
            if(t.category):
                breadcrumb.append([t.category.name, '/c/' + t.category.short_name])
            breadcrumb.append([t.name, '/t/' + t.short_name])
        elif (section == 'e'):
            e = get_object_or_404(Entry, id=identifier)
            t = e.tags.first()
            if(t.category):
                breadcrumb.append([t.category.name, '/c/' + t.category.short_name])
            breadcrumb.append([t.name, '/t/' + t.short_name])
            breadcrumb.append([e, '/e/' + str(e.id)])
        elif (section == 'p'):
            try: 
                i = Item.objects.get(short_name=identifier)
                breadcrumb.append([i.parent.name, '/m/' + i.parent.short_name])
                breadcrumb.append([i.name, '/p/' + identifier])
            except Item.DoesNotExist:
                if apps.is_installed('simple_pages' ):
                    p = get_object_or_404(Page, short_name=identifier)
                    breadcrumb.append([p.name, '/p/' + identifier])
        elif (section == 'm'):
            i = get_object_or_404(Item, short_name=identifier)
            breadcrumb.append([i.name, '/m/' + i.short_name])
        else:
            # If is another kind of URL, it is an app with its corresponding name
            identifier = path.split('/')[1]
            try:
                # If app is in the menu, search its parent
                i = Item.objects.get(short_name=identifier)
                breadcrumb.append([i.parent.name, '/m/' + i.parent.short_name])
                breadcrumb.append([i.name, identifier])
            except ObjectDoesNotExist:
                # If app is not in the menu, write it directly.
                # The name of the app in breadcrumb will be the identifier capitalized
                breadcrumb.append([identifier.capitalize(), identifier])
    return breadcrumb

def get_visits(request):
    token = settings.MATOMO_TOKEN
    workgroup_id = settings.WORKGROUP_ID
    url = 'https://matomo.sindicat.net/?module=API&method=Actions.getPageUrl&pageUrl=http://' + \
          request.META.get('SERVER_NAME') + \
          request.path + \
          '&idSite=' + workgroup_id + \
          '&date=today&period=day&format=json&token_auth=' + token
    r = requests.get(url=url).json()
    try:
        visits = r[0]['nb_visits']
    except (IndexError, KeyError):
        visits = '-'
    return visits