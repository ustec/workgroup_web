/* When a video is from Youtube or from Vimeo, url field will be disabled, 
 * an id must be set.  If video is from anywhere else, the correpsonding URL 
 * must be set. 
 */

django.jQuery(document).ready(function($){	
	function set_disabled_url_id(){
	    video_type = $("#id_type").val();
	    if(video_type == "PEERTUBE" || video_type == "YOUTUBE" || video_type == "VIMEO") {
	        $("#id_video_id").prop('disabled', false);
	        $("#id_url").val("").prop('disabled', true);
	    } else {
	        $("#id_video_id").val("").prop('disabled', true);
            $("#id_url").prop('disabled', false);
	    }
    }
	set_disabled_url_id();
	$("#id_type").change(set_disabled_url_id);
});

