// Hide all fields and labels related to membership (for no members)
function hide_membership_form(){
    $('#id_birthday_day').parent().hide();
    $('#id_address').parent().hide();
    $('#id_postal_code').parent().hide();
    $('#id_city').parent().hide();
    $('#id_school_name').parent().hide();
    $('#id_school_city').parent().hide();    
    $('#id_school_code').parent().hide();
    $('#id_current_speciality').parent().hide();
    $('#id_situation').parent().hide();
    $('#id_iban').parent().hide();
}

// Show all fields and labels related to membership (for no members)
function show_membership_form(){
    $('#id_birthday_day').parent().show();
    $('#id_address').parent().show();
    $('#id_postal_code').parent().show();
    $('#id_city').parent().show();
    $('#id_school_name').parent().show();
    $('#id_school_city').parent().show();
    $('#id_school_code').parent().show();
    $('#id_current_speciality').parent().show();
    $('#id_situation').parent().show();
    $('#id_iban').parent().show();
}

// When page loads, do not show membership fields, unless "no member" is selected
$(document).ready(function(){
   if ($("#id_is_real_member").val() == 'False'){
        show_membership_form();
    } else {
        hide_membership_form();
    }
});
