$(document).ready(function(){
    /* Configure poster carousels with slick */
    // Remove default dots and add a custom class
    $('.poster-carousel').on('init', function(event, slick){
        var $items = slick.$dots.find('li');
        $items.addClass('circle');
        $items.find('button').remove();
    });
    // Slick carousel options
    $('.poster-carousel').slick({
        dots: true,
        autoplaySpeed: 5000,
        autoplay: true,
        adaptiveHeight: true
    });

    /* Login */
    $('#login').submit(function(){
        var url = 'https://' + $('#user').val() + ':' + $('#password').val() + '@antiga.sindicat.net/espacioseguro/zona/clau.php';
        location.href=url;
        return false;
    });
    
    /* Search */
    $('#search').submit(function(){
        // Add sites to the search input
        var sites = " site:sindicat.net"
        $("#q").val($("#q").val() + sites);
        return true;
    });
    $(document).ready(function(){
        $("#q").val("")
    });
});

