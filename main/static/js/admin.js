django.jQuery(document).ready(function($){	
	function set_tag_disabled_elems(){
		if($("#id_is_hidden").prop("checked")) {
	        $("#id_color").val("").prop('disabled', true);
	        $('#id_category option:eq(0)').prop('selected', true);
	        $("#id_category").prop('disabled', true);
	    } else {
	    	$("#id_color").prop('disabled', false);
	    	$("#id_category").prop('disabled', false);
	    }
		
	}
	set_tag_disabled_elems()
	$("#id_is_hidden").change(set_tag_disabled_elems);
});

