django.jQuery(document).ready(function($){
	/* 
	 * Entry administration: when entry is not featured, set featured order read only 
	 * and set it to 0. Remove readonly property otherwise. 
	 */  
	function toggle_featured_order(){
	    // Show or hide order depending if entry is featured or not
        if($("#id_is_featured").prop("checked")) {  
            $(".form-row.field-featured_order").show();    
        } else {
            $("#id_featured_order").val("0");
            $(".form-row.field-featured_order").hide();
        }
    }
	
	// Hide up an down button of entries list when entry is featured
	function hide_up_down_buttons(){	    
	    $( ".field-move_up_down_links" ).each(function( i ) {	        
	        image_featured = $(this).next().children()[0]
	        if (image_featured.alt == "True"){
	                $(this).children().hide()
	        }
	     });
	}
	
	// Hide order number when entry is not featured
    function hide_featured_order_number(){        
        $( ".field-featured_order" ).each(function( i ) {           
            image_featured = $(this).prev().children()[0]
            if (image_featured.alt == "False"){
                    $(this).text("")
            }
         });
    }
	
	// Call functions
	hide_up_down_buttons();
	hide_featured_order_number();
	toggle_featured_order();
	$("#id_is_featured").change(toggle_fetured_order);
	
});

