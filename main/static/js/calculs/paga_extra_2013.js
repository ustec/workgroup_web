$(document).ready(function(){
    /* Calculate paycheck percentage */
    function get_paycheck(){
        var c005T = parseFloat($('#005T').val().replace(",", "."));
        var c085T = parseFloat($('#085T').val().replace(",", "."));
        var perc = parseFloat($('#perc').val());
        if (perc == 30)
            paycheck = (c005T + c085T) * perc / 10.;
        else if (perc = 60)
            paycheck = (c005T + c085T) * perc / 30.;
        if (isNaN(paycheck)){
            return 0;
        } else {
            return paycheck.toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              });            
        }        
    }
    
    /* Calculate corresponding percentages while typing */
    $('#005T').on('input',function () {
        $("#paycheck").text(get_paycheck());
    });
    $('#085T').on('input',function () {
        $("#paycheck").text(get_paycheck());
    });
 });