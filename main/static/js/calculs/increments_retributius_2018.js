$(document).ready(function(){
    
    // Initialize form and totals
    $('#salary').val('');
    setTotal('#incOct', 0);
    setTotal('#incNov', 0);
    setTotal('#incDec', 0);
    setTotal('#incJan', 0);
    
    // Set total with correct decimal symbol and 2 decimals
    function setTotal(id, number){
        $(id).text(number.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }));
    }
    
    $('#salary').keyup(function () {
        var salary = $('#salary').val();
        if (salary != ''){
            var incOct = 0.0175 * 4 * salary;
            var incNov = 0.015 * 6.85 * salary;
            var incDec = 0.0175 * salary + 70.56;
            var incJan   = 0.0175 * salary + 5.04;            
            setTotal('#incOct', incOct);
            setTotal('#incNov', incNov);
            setTotal('#incDec', incDec);
            setTotal('#incJan', incJan);
        } else {
            setTotal('#incOct', 0);
            setTotal('#incNov', 0);
            setTotal('#incDec', 0);
            setTotal('#incJan', 0);
        }
    });
 });