﻿/*
 Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
*/
CKEDITOR.addTemplates("default",
    {
    imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),
    templates:[
        {title:"2 Columnes 66% 33%",
            image:"template2.gif",
            description:"Dues columnes: columna esquerra ocupant 2/3 i columna dreta ocupant 1/3",
            html:' <div class="row"><div class="col-sm-8">Columna esquerra</div><div class="col-sm-4">Columna dreta</div></div> '
         },
]
}
);
