class AppsRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'posters' or \
           model._meta.app_label == 'videos' or \
           model._meta.app_label == 'main_menu':
            return 'common'
        elif model._meta.app_label == 'departament' or \
             model._meta.app_label == 'nomenaments' or \
             model._meta.app_label == 'dades_plantilles' or \
             model._meta.app_label == 'especialitat_borsa':
            return 'departament'
        elif model._meta.app_label == 'assessoraments':
            return 'assessoraments'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'posters' or \
           model._meta.app_label == 'videos' or \
           model._meta.app_label == 'main_menu':
            return 'common'
        elif model._meta.app_label == 'departament' or \
             model._meta.app_label == 'nomenaments' or \
             model._meta.app_label == 'dades_plantilles' or \
             model._meta.app_label == 'especialitat_borsa':
            return 'departament'
        elif model._meta.app_label == 'assessoraments':
            return 'assessoraments'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'posters' or \
           obj2._meta.app_label == 'posters':
            return True
        if obj1._meta.app_label == 'videos' or \
           obj2._meta.app_label == 'videos':
            return True
        if obj1._meta.app_label == 'main_menu' or \
           obj2._meta.app_label == 'main_menu':
            return True
        if obj1._meta.app_label == 'departament' or \
           obj2._meta.app_label == 'departament':
            return True
        if obj1._meta.app_label == 'nomenaments' or \
           obj2._meta.app_label == 'nomenaments':
            return True
        if obj1._meta.app_label == 'dades_plantilles' or \
           obj2._meta.app_label == 'dades_plantilles':
            return True        
        if obj1._meta.app_label == 'especialitat_borsa' or \
           obj2._meta.app_label == 'especialitat_borsa':
            return True
        if obj1._meta.app_label == 'assessoraments' or \
           obj2._meta.app_label == 'assessoraments':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'posters' or \
           app_label == 'videos' or \
           app_label == 'main_menu':
            return db == 'common'
        elif app_label == 'departament' or \
             app_label == 'nomenaments' or \
             app_label == 'dades_plantilles' or \
             app_label == 'especialitat_borsa':
            return db == 'departament'
        elif app_label == 'assessoraments':
            return db == 'assessoraments'
        if db == 'common' or \
            db == 'departament' or \
            db == 'nomenaments' or \
            db == 'assessoraments':
            return False
        return True
