from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import Entry
        
@receiver(pre_save, sender=Entry)
def reorder_entry(sender, instance, update_fields, *args, **kwargs):
    # Get poster before being saved
    try:
        entry_old = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        entry_old = None
    # Disconnect this method from presave to avoid infinite loop saving
    # top and bottom methods call save method
    pre_save.disconnect(reorder_entry, sender=Entry)
    # Move new entry to first position
    if entry_old is None:
        instance.top()
    # Connect to presave again
    pre_save.connect(reorder_entry, sender=Entry)