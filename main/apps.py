from django.apps import AppConfig

class MainConfig(AppConfig):
    name = 'main'
    verbose_name = 'Grup de treball'
    def ready(self):
        import main.signals