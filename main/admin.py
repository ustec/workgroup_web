from django import forms
from django.contrib import admin
from ordered_model.admin import (OrderedInlineModelAdminMixin,
                                 OrderedModelAdmin, OrderedTabularInline)

from .models import Category, Entry, Tag
from .validations import Validator


## Category administration

class CategoryForm(forms.ModelForm):
    def clean_short_name(self):
        short_name = self.cleaned_data.get('short_name')
        if not Validator.valid_short_name(short_name):
            raise forms.ValidationError("Nom curt incorrecte. Només pot contenir lletres, números, - i _")
        return self.cleaned_data['short_name']

class TagInline(OrderedTabularInline):
    model = Tag
    fields = ('name', 'move_up_down_links',)
    readonly_fields = ('name', 'move_up_down_links',)
    ordering = ('order',)
    # This inline form is only for reordering.
    # To add, delete or edit labels, use TagAdmin
    # Removing tags is not allowed
    can_delete = False
    # Adding new tags is not allowed
    max_num = 0

class CategoryAdmin(OrderedInlineModelAdminMixin, OrderedModelAdmin):
    form = CategoryForm
    list_display = ('name', 'move_up_down_links', 'visible')
    list_filter = ('visible',)
    inlines = (TagInline, )

    def get_urls(self):
        urls = super(CategoryAdmin, self).get_urls()
        for inline in self.inlines:
            if hasattr(inline, 'get_urls'):
                urls = inline.get_urls(self) + urls
        return urls

## Tag administration

class TagForm(forms.ModelForm):
    def clean_short_name(self):
        short_name = self.cleaned_data.get('short_name')
        if not Validator.valid_short_name(short_name):
            raise forms.ValidationError("Nom curt incorrecte. Només pot contenir lletres, números, - i _")
        return self.cleaned_data['short_name']

class TagAdmin(admin.ModelAdmin):
    # List name and category of tags
    list_display = ('name', 'category')
    # Allow filtering tags by category
    list_filter = ('category',)
    form = TagForm
    class Media:
        js = ("js/admin.js",)

## Entry administration

class EntryAdmin(OrderedModelAdmin):
    list_display = ('__str__', 'move_up_down_links','is_featured','featured_order')
    date_hierarchy = 'date_time'
    # Improve tag selection
    filter_horizontal = ('tags',)
    # Add some filtersb
    list_filter = ('tags', 'is_featured', 'is_in_front_page','show_in_app')
    ordering = ('-is_featured','featured_order','order')
    class Media:
        js = ("js/admin_main_entry.js",)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(Tag, TagAdmin)