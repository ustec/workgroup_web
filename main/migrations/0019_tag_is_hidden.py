# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_auto_20171024_1215'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='is_hidden',
            field=models.BooleanField(verbose_name='Etiqueta oculta', default=False),
        ),
    ]
