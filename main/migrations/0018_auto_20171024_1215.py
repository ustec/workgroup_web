# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_auto_20171024_1153'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tag',
            old_name='colour',
            new_name='color',
        ),
    ]
