# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_category_body'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='is_important',
            field=models.BooleanField(default=False, verbose_name='És important'),
            preserve_default=False,
        ),
    ]
