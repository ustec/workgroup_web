# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-10-28 09:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_auto_20181107_1208'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='entry',
            options={'ordering': ('order',), 'verbose_name': 'entrada', 'verbose_name_plural': 'entrades'},
        ),
        migrations.AddField(
            model_name='entry',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
    ]
