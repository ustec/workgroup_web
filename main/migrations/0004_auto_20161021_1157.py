# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_entry_date_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='body',
            field=models.TextField(blank=True),
        ),
    ]
