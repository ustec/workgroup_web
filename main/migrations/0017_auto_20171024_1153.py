# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_tag_colour'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='colour',
            field=models.CharField(verbose_name='Color', blank=True, max_length=7),
        ),
    ]
