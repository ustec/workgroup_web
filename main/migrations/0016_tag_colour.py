# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_entry_is_important'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='colour',
            field=models.CharField(max_length=7, verbose_name='Color', default=''),
            preserve_default=False,
        ),
    ]
