# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20161021_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='body',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True),
        ),
        migrations.AlterField(
            model_name='entry',
            name='summary',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
