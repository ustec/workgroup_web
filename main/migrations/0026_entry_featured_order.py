# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-07 10:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_auto_20180312_1223'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='featured_order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Ordre entrada destacada'),
        ),
    ]
