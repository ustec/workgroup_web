# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_category_short_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tag',
            old_name='long_name',
            new_name='name',
        ),
    ]
