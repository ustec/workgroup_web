# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='main_image',
            field=models.ImageField(upload_to='main_images/', blank=True),
        ),
    ]
