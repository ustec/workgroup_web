# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20161019_0724'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='date_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 19, 9, 13, 49, 195639, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
