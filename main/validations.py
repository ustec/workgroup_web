import re

class Validator:
    @staticmethod
    def valid_short_name(short_name):
        if re.search("^[a-z0-9\-_]+$", short_name):
            return True
        return False