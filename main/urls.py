from django.conf import settings
from django.conf.urls import url
from django.urls import path, re_path, include
from django.conf.urls.static import static
from . import views
from main.views import robots_txt

urlpatterns = [
    #url(r'^$', views.index),
    path('', views.index),
    #url(r'^robots.txt', robots_txt),
    path('robots.txt', robots_txt),
    #url(r'^c/(?P<short_name>[0-9a-z\-]+)/$', views.entries_by_category_list),
    path('c/<slug:short_name>/', views.entries_by_category_list),
    #url(r'^t/(?P<short_name>[0-9a-z\-]+)/$', views.entries_by_tag_list),
    path('t/<slug:short_name>/', views.entries_by_tag_list),
    #url(r'^e/(?P<id>[0-9]+)/$', views.entry),
    path('e/<int:id>/', views.entry),
    #url(r'^json/app_entries$', views.get_app_entries_json),
    re_path('^json/app_entries$', views.get_app_entries_json),
    #url(r'^json/tags$', views.get_tags_json),
    re_path('^json/tags$', views.get_tags_json),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static(settings.COMMON_MEDIA_URL, document_root=settings.COMMON_MEDIA_ROOT)