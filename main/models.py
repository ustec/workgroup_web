from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
from ordered_model.models import OrderedModel
import html
import re

# Entries tags
class Tag(OrderedModel):
    short_name = models.CharField(max_length=50, verbose_name='Nom curt')
    name = models.CharField(max_length=100, verbose_name='Nom')
    body = RichTextUploadingField(blank=True, verbose_name='Cos')
    # Tags that are not in a category will not be listed in the menu
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Categoria')
    color = models.CharField(max_length=7, blank=True, verbose_name='Color')
    is_hidden = models.BooleanField(default=False, verbose_name='Etiqueta oculta')
    order_with_respect_to = 'category'
    class Meta(OrderedModel.Meta):
        verbose_name = 'etiqueta'
        verbose_name_plural = 'etiquetes'
        ordering = ['order']
    def __str__(self):
        return self.name

# Tags are in a category
class Category(OrderedModel):
    short_name = models.CharField(max_length=50, verbose_name='Nom curt')
    name = models.CharField(max_length=50, verbose_name='Nom')
    body = RichTextUploadingField(blank=True, verbose_name='Cos')
    visible = models.BooleanField(default=True)
    class Meta(OrderedModel.Meta):
        verbose_name = 'categoria'
        verbose_name_plural = 'categories'
    def tags(self):
        return Tag.objects.filter(category=self.id)
    def __str__(self):
        return self.name

# Class with any content
class Entry(OrderedModel):
    summary = RichTextUploadingField(config_name='basic', verbose_name='Resum')
    body = RichTextUploadingField(blank=True, verbose_name='Cos')
    main_image = models.ImageField(upload_to='main_images/', blank=True, verbose_name='Imatge Principal')
    is_featured = models.BooleanField(verbose_name='Entrada destacada')
    featured_order = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name="Ordre entrada destacada")
    is_in_front_page = models.BooleanField(default=True, verbose_name='Està a la pàgina principal')
    show_in_app = models.BooleanField(default=True, verbose_name='Mostra a l\'app')
    tags = models.ManyToManyField(Tag, verbose_name='Etiquetes')
    date_time = models.DateTimeField(default=timezone.now, verbose_name='Data i hora')
    class Meta(OrderedModel.Meta):
        verbose_name = 'entrada'
        verbose_name_plural = 'entrades'
    
    # Check if all tags of this entry are hidden. If so, date will not be shown
    def all_tags_hidden(self):
        for tag in self.tags.all():
            if not tag.is_hidden:
                return False
        return True
    
    def __str__(self):
        # Regular expression to search tags
        p = re.compile(r'<.*?>')
        # Remove tags in summary and decode HTML entities and references
        summary_without_tags = html.unescape(p.sub('', self.summary))
        # Return the resulting string shortened to 100 characters
        return summary_without_tags[0:100] + "..." if len(summary_without_tags)>100 else summary_without_tags
