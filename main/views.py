from django.apps import apps
from django.conf import settings
from django.shortcuts import get_object_or_404, render, redirect
from django.http import JsonResponse
from .models import Category
from .models import Entry
from .models import Tag
from .utils import get_common_info, get_entries_per_page
from django.http.response import HttpResponse
from django.views.decorators.http import require_GET

def index(request):
    # Disable web if vaga
    if apps.is_installed('config' ):
        Config = apps.get_model('config', 'Config')
        vaga_general = Config.objects.filter(key='VAGA_GENERAL').first()
        if vaga_general is not None and vaga_general.value != '':
            # If URL is sindicat.net?entra, allow to visit the web
            if not request.META.get('QUERY_STRING') == 'entra':
                return redirect(vaga_general.value);
    # Get common info of all templates
    context = get_common_info(request)
    # Get the rest of entries
    all_entries = Entry.objects.all().filter(is_in_front_page=True).order_by("-is_featured","featured_order","order")
    # Get which page we must display and get the corresponding entries
    page = request.GET.get('page')
    entries = get_entries_per_page(page, all_entries)
    if apps.is_installed('nomenaments') and apps.is_installed('departament'):
        context.update(nomenaments_installed=True)
        ServeiTerritorial = apps.get_model('departament', 'ServeiTerritorial')
        sts = ServeiTerritorial.objects.all()
        context.update(sts=sts)
        NomenamentNumero = apps.get_model('nomenaments', 'NomenamentNumero')
        nomenaments_num_today = NomenamentNumero.get_nomenaments_num_dict()
        context.update(nomenaments_num=nomenaments_num_today)
    # Show or not right column depending on settings
    if hasattr(settings, 'RIGHT_COLUMN'):
        show_right_column = settings.RIGHT_COLUMN
    else:
        show_right_column = True
    context.update(show_right_column=show_right_column)
    # Return data to the template
    context.update(entries=entries)
    return render(request, 'main/index.html', context)

def entries_by_tag_list(request, short_name):
    tag = Tag.objects.get(short_name=short_name)
    # Get common info of all templates
    context = get_common_info(request)
    # Get entries with tag ordered by date_time in descending order
    all_entries = Entry.objects.filter(tags__in=[tag]).order_by("-is_featured","featured_order","order")
    # Get which page we must display and get the corresponding entries
    page = request.GET.get('page')
    entries = get_entries_per_page(page, all_entries)
    # Return data to the template
    context.update(entries=entries, current_tag=tag)
    return render(request, 'main/entries_by_tag_list.html', context)

def entries_by_category_list(request, short_name):
    category = Category.objects.get(short_name=short_name)
    # Get common info of all templates
    context = get_common_info(request)
    # Get all category tags
    tags = Tag.objects.filter(category=category)
    # Get entries ordered by date_time in descending order
    all_entries = Entry.objects.filter(tags__in=tags).order_by("-is_featured","featured_order","order").distinct()
    # Get which page we must display and get the corresponding entries
    page = request.GET.get('page')
    entries = get_entries_per_page(page, all_entries)
    # Return data to the template
    context.update(entries=entries, current_category=category)
    return render(request, 'main/entries_by_category_list.html', context)

def entry(request, id):
    # Get common info of all templates
    context = get_common_info(request)
    entry = get_object_or_404(Entry, id=id)
    # Return data to the template
    context.update(entry=entry)
    return render(request, 'main/entry.html', context)


# Return entries to be shown in app in JSON format 
def get_app_entries_json(request):
    # Get all important entries
    all_entries = Entry.objects.filter(show_in_app=True).order_by("-is_featured","featured_order","order")
    # Build a dictionary with the summary and body of these entries
    d = []
    for e in all_entries:
        v = {}
        v['pk'] = e.pk
        v['summary'] = e.summary
        v['body'] = e.body
        v['is_featured'] = e.is_featured
        tags = []
        for tag in e.tags.all():
            t = {}
            t['name'] = tag.name
            t['color'] = tag.color
            tags.append(t)
        v['tags'] = tags
        d.append(v)
    return JsonResponse(d, safe=False)

def get_tags_json(request):
    # Get all tags
    all_tags = Tag.objects.all()
    # Build a list with all tag names and colors
    tags_list = []
    for tag in all_tags:
        t = {}
        t['name'] = tag.name
        t['color'] = tag.color
        tags_list.append(t)
    return JsonResponse(tags_list, safe=False)

def redirect_old_web(request, exception=None):
    # Any page of style http://sindicat.net/lala that is not found 
    # will be redirected to http://antiga.sindicat.net/lala
    return redirect(settings.OLD_WEB + request.get_full_path())

@require_GET
def robots_txt(request):
    lines = [
        "User-Agent: *",
        "Disallow: /config/",
    ]
    return HttpResponse("\n".join(lines), content_type="text/plain")