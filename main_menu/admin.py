from django.contrib import admin
from .models import Item, ExternalLink
from ordered_model.admin import OrderedModelAdmin, OrderedTabularInline

class ItemInline(OrderedTabularInline):
    model = Item
    readonly_fields = ( 'move_up_down_links',)
    exclude = ('icon',)
    ordering = ('order',)
     
class ItemAdmin(OrderedModelAdmin):
    list_display = ('name', 'move_up_down_links')
    inlines = (ItemInline, )
    
    # Only show first level items
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(parent_id=None)
     
    def get_urls(self):
        urls = super(ItemAdmin, self).get_urls()
        for inline in self.inlines:
            if hasattr(inline, 'get_urls'):
                urls = inline.get_urls(self) + urls
        return urls

admin.site.register(Item, ItemAdmin)
admin.site.register(ExternalLink)
