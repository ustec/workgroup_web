from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from ordered_model.models import OrderedModel

common_media_storage = FileSystemStorage(location=settings.COMMON_MEDIA_ROOT, \
                                         base_url=settings.COMMON_MEDIA_URL)

class Item(OrderedModel):
    short_name = models.CharField(max_length=50, 
                                  verbose_name='Nom curt')
    name = models.CharField(max_length=100, 
                            verbose_name='Nom')
    PAGE = 'PAGE'
    APP = 'APP'
    WORKGROUP = 'WORKGROUP'
    EXTERNAL = 'EXTERNAL'
    NOLINK = 'NOLINK'
    ITEM_TYPE_CHOICES = (
        (PAGE, 'Pàgina interna de la web principal'),
        (APP, 'Aplicatiu de la web principal'),
        (WORKGROUP, 'Web d\'un grup de treball'),        
        (EXTERNAL, 'Enllaç extern'),
        (NOLINK, 'Sense enllaç'),
    )
    type = models.CharField(max_length=100,
                            choices=ITEM_TYPE_CHOICES, 
                            default=PAGE, 
                            verbose_name='Tipus d\'enllaç')
    icon = models.FileField(blank=True, null=True,
                             storage=common_media_storage, \
                             upload_to='main_menu_icons/', \
                             verbose_name='Icona')
    link = models.URLField(max_length=300,
                           blank=True, null=True,
                           verbose_name='Enllaç')
    # If parent is null, item is in the first level
    parent = models.ForeignKey('Item',
                               on_delete=models.CASCADE, 
                               blank=True, null=True, 
                               verbose_name='Ítem pare')
    order_with_respect_to = 'parent'
    class Meta(OrderedModel.Meta):
        verbose_name = 'ítem'
        verbose_name_plural = 'ítems'
    def __str__(self):
        return self.name
    
class ExternalLink(OrderedModel):
    name = models.CharField(max_length=100, 
                        verbose_name='Nom')
    link = models.URLField(max_length=300,
                       verbose_name='Enllaç')
    class Meta(OrderedModel.Meta):
        verbose_name = 'Enllaç extern'
        verbose_name_plural = 'Enllaços externs'
    def __str__(self):
        return self.name