from django.shortcuts import get_object_or_404, render
from .models import Item
from main.utils import get_common_info

def main_submenu(request, short_name):
    current_item = get_object_or_404(Item, short_name=short_name)
    submenu = Item.objects.filter(parent=current_item.id)
    # Get common info of all templates
    context = get_common_info(request)
    context.update(current_item=current_item)
    context.update(submenu=submenu)
    # Return data to the template
    return render(request, 'main_menu/main_submenu.html', context)