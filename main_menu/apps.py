from django.apps import AppConfig


class MainMenuConfig(AppConfig):
    name = 'main_menu'    
    verbose_name = 'Menú principal'
