from collections import OrderedDict
from .models import Item, ExternalLink

def get_main_menu():
    # Use an OrderedDict, so items will be presented in he order they are added to the dict
    main_menu = OrderedDict()
    first_level_items = Item.objects.filter(parent=None)
    for item in first_level_items:
        main_menu[item] = Item.objects.filter(parent=item.id)
    return main_menu

def get_external_links():
    external_links = ExternalLink.objects.all().order_by("-order")
    return external_links