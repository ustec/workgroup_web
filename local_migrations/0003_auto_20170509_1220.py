# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.files.storage
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

        dependencies = [
            ('posters', '0002_auto_20170508_0943'),
        ]

        operations = [
            migrations.AlterModelOptions(
                name='poster',
                options={'verbose_name_plural': 'cartells', 'verbose_name': 'cartell'},
           ),
           migrations.AddField(
               model_name='poster',
               name='date_time',
               field=models.DateTimeField(default=datetime.datetime(2017, 5, 9, 10, 20, 27, 150325, tzinfo=utc), auto_now_add=True),
               preserve_default=False,
           ),
           migrations.AlterField(
               model_name='poster',
               name='poster',
               field=models.ImageField(upload_to='posters/', storage=django.core.files.storage.FileSystemStorage(location='./media/common/', base_url='/media/common/'), verbose_name='Cartell'),
           ),
    ]
