from django import template

register = template.Library()

@register.filter(name='getdictvalue')
def getdictvalue(value):
    dict_sts = {
        '0108': 'Consorci Barcelona',
        '0117': 'Girona',
        '0125': 'Lleida',
        '0143': 'Tarragona',
        '0208': 'Barcelona Comarques',
        '0243': 'Terres de l\'ebre',
        '0308': 'Baix Llobregat',
        '0408': 'Vallès Occidental',
        '0508': 'Maresme Vallès Or.',
        '1060': 'Catalunya Central',
        }
    value = dict_sts[value]
    return value