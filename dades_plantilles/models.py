from django.db import models

# Create your models here.

class Plantilla(models.Model):

    codi_st = models.CharField(max_length=10,null=True)
    codi_centre = models.CharField(max_length=10,null=True)
    tipus_centre = models.CharField(max_length=100,null=True)
    nom_centre = models.CharField(max_length=100,null=True)
    municipi = models.CharField(max_length=100,null=True)
    complexitat = models.CharField(max_length=100,null=True)
    especial_dificultat = models.CharField(max_length=100,null=True)
    total_ocu_definitiva = models.CharField(max_length=100,null=True)
    total_dotacio = models.CharField(max_length=100,null=True)
    codi_destinacio = models.CharField(max_length=100,null=True)
    qualificador = models.CharField(max_length=100,null=True)
    destinacio_definitiva = models.CharField(max_length=100,null=True)
    comissio_serveis = models.CharField(max_length=100,null=True)
    destinacio_provisional = models.CharField(max_length=100,null=True)
    provisional_x_supressio = models.CharField(max_length=100,null=True)
    indefinit_no_fix = models.CharField(max_length=100,null=True)
    interi = models.CharField(max_length=100,null=True)