# Generated by Django 2.2.17 on 2021-01-27 05:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dades_plantilles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plantilla',
            name='total_dotacio',
            field=models.CharField(max_length=20),
        ),
    ]
