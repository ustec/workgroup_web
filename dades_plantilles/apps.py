from django.apps import AppConfig


class DadesPlantillesConfig(AppConfig):
    name = 'dades_plantilles'
