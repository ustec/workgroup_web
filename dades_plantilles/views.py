from django.shortcuts import render
from dades_plantilles.models import Plantilla

def index(request):
    context = {}
    
    # User selected ST or default
    try:
        selected_st = request.GET['codiservei']
    except:
        # if no selection first item selected
        selected_st = Plantilla.objects.values('codi_st').filter(codi_st="0108")[:1]
        selected_st = selected_st[0]['codi_st']
    context.update(selected_st=selected_st)
    
    # User selected municipi or default 
    try:
        selected_municipi = request.GET['municipi']
    except:
        # if no selection first item selected
        selected_municipi= Plantilla.objects.filter(codi_st=selected_st).values('municipi').distinct().order_by('municipi')[:1]
        selected_municipi = selected_municipi[0]['municipi']
    else:
        st_nou_municipi = Plantilla.objects.values('codi_st').filter(municipi=selected_municipi)[:1]
        st_nou_municipi = st_nou_municipi[0]['codi_st']
        st_municipi= Plantilla.objects.filter(codi_st=selected_st).values('municipi').distinct().order_by('municipi')[:1]
        st_municipi = st_municipi[0]['municipi']

        if st_nou_municipi != selected_st: # if user change st
            selected_municipi= Plantilla.objects.filter(codi_st=selected_st).values('municipi').distinct().order_by('municipi')[:1]
            selected_municipi = selected_municipi[0]['municipi']
    context.update(selected_municipi=selected_municipi)
    
    # User selected tipus de centre or default 
    try:
        selected_t_centre = request.GET['t_centre']
    except:
        # if no selection first item selected
        selected_t_centre = Plantilla.objects.values('tipus_centre').filter(tipus_centre='ESCOLA')[:1]
        selected_t_centre = selected_t_centre[0]['tipus_centre']
    context.update(selected_t_centre=selected_t_centre)
    
    # User selected codi destinacio or default 
    try:
        selected_c_desti = request.GET['c_desti']
    except:
        # if no selection first item selected
        selected_c_desti = Plantilla.objects.values('codi_destinacio').filter(codi_destinacio='EDUCACIÓ PRIMÀRIA')[:1]
        selected_c_desti = selected_c_desti[0]['codi_destinacio']
    context.update(selected_c_desti=selected_c_desti)
    
    # User centre educatiu or none
    try:
        selected_centre = request.GET['n_centre']
        filtrat_x_centre = True
    except:
        # if no selection sense filtrar per centre
        selected_centre = "Tots"
        filtrat_x_centre = False

    # Get all serveis territorials (st) 
    serv_terr = Plantilla.objects.values('codi_st').distinct().order_by('codi_st').distinct().order_by('codi_st')
    context.update(serv_terr=serv_terr)
    
    # Get all municipis
    municipis = Plantilla.objects.filter(codi_st=selected_st).values('municipi').distinct().order_by('municipi')
    context.update(municipis=municipis)
        
    # Get all tipus de centre
    t_centre = Plantilla.objects.values('tipus_centre').distinct().order_by("tipus_centre")
    context.update(t_centre=t_centre)
    
    # Get all codis destinacio filtered by tipus de centre
    c_destinacio = Plantilla.objects.filter(tipus_centre=selected_t_centre).values('codi_destinacio').distinct().order_by('codi_destinacio')
    context.update(c_destinacio=c_destinacio)

    # Get all centres filtered by municipi
    centres = Plantilla.objects.filter(municipi=selected_municipi).values('nom_centre').distinct().order_by('nom_centre')
    context.update(centres=centres)

    if filtrat_x_centre and selected_centre != 'tots':
        plantilla_filtered = Plantilla.objects.all().filter(nom_centre__exact=selected_centre, municipi__exact=selected_municipi)
    else:
        plantilla_filtered = Plantilla.objects.all().filter(municipi__exact=selected_municipi, tipus_centre=selected_t_centre, codi_destinacio=selected_c_desti)
    
    context.update(plantilla_filtered=plantilla_filtered)


    return render(request, 'dades_plantilles/index.html', context)
